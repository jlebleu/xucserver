.. highlightlang:: rest

.. _statistics:

#########
Satistics
#########

==============
Exposed by xuc
==============



These real time statistics are calculated nearly in real time from the queue_log table
Statistic are reset to 0 at midnight (24h00) can be changed by configuration

Real time calculated Queue statistic
------------------------------------

+------------------------------------+------------------------------------------------------------+
| name                               | Description                                                |
+====================================+============================================================+
| TotalNumberCallsEntered            | Total number of calls entered in a queue                   |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAbandonned         | Total number of calls abandoned in a queue (not answered)  |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAbandonnedAfter15  | Total number of calls abandoned after 15 seconds           |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAnswered           | Total number of calls answered                             |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsAnsweredBefore15   | Total number of calls answered before 15 seconds           |
+------------------------------------+------------------------------------------------------------+
| PercentageAnsweredBefore15         | Percentage of calls answered before 15 seconds             |
|                                    | over total number of calls entered                         |
+------------------------------------+------------------------------------------------------------+
| PercentageAbandonnedAfter15        | Percentage of calls abandoned after 15 seconds             |
|                                    | over total number of calls entered                         |
+------------------------------------+------------------------------------------------------------+
| TotalNumberCallsClosed             | Total number or calls received when queue is closed        |
+------------------------------------+------------------------------------------------------------+

Other queue statistics
----------------------

Other queue statistics are calculated by xivo cti server

* AvailableAgents
* TalkingAgents
* LongestWaitTime
* WaitingCalls
* EWT

Definition in xivo documentation `xivo documentation <http://documentation.xivo.fr/production/contact_center/supervision/supervision.html?highlight=ewt#queue-list>`_

