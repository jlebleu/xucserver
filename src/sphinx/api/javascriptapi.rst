.. javascriptapi:

##############
Javascript API
##############


Introduction
============

The Xuc javascript API enables you to integrate enterprise communication functions to your business application.
It exposes Cti functions using javascript methods calls.

You may add your own handlers for your application to react to telephony / contact center events.

This API is using websockets_, and therefore needs a modern browser supporting them (firefox_, chrome ...)

.. _firefox: https://www.mozilla.org/en-US/firefox/desktop/ 
.. _websockets: http://en.wikipedia.org/wiki/WebSocket

Integration Principles
======================

* Include the Cti javascript API from the Xuc Server

::

   <script src="http://<xucserver>:<xucport>/assets/javascripts/cti.js" type="text/javascript"></script>
   
* Connect to the Xuc serveur using XiVO client username and password

::

   var wsurl = "ws://"+server+"/ctichannel?username="+username+"&amp;agentNumber="+phoneNumber+"&amp;password="+password; 
   Cti.WebSocket.init(wsurl,username,phoneNumber);
   
* Setup event handlers to be nofified on
   * Phone state changes
   * Agent state changes
   * Statistics
   * ...

* Once web socket communication is established you are able to call XuC Cti javascript methods.
   * Place a call, log an agent ....

::

      ...
      $('#login_btn').click(function(event){ 
         Cti.loginAgent($('#agentPhoneNumber').val()); 
      }); 
      $('#logout_btn').click(function(event){ 
         Cti.logoutAgent(); 
      }); 
      $('#xuc_dial_btn').click(function(event){ 
         Cti.dial($("#xuc_destination").val());
      });
      ...

Sample Application
==================

A sample application is provided by the XuC server. This application allows to display events and using different methods exposed by the XuC

::

   http://<sucserver>:<xucport>/sample

.. figure:: sampleapp.png
   :scale: 90%

You may browse and use the ``sample.js`` javascript file as an example

* Calling Cti methods :

::

   .$('#xuc_login_btn').click(function(event) {
        Cti.loginAgent($('#xuc_agentPhoneNumber').val());
    });

    $('#xuc_logout_btn').click(function(event) {
        Cti.logoutAgent();
    });
    $('#xuc_pause_btn').click(function(event) {
        Cti.pauseAgent();
    });
    $('#xuc_unpause_btn').click(function(event) {
        Cti.unpauseAgent();
    });
    $('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
        Cti.subscribeToQueueStats();
    });
    $('#xuc_answer_btn').click(function(event) {
        Cti.answer();
    });
    $('#xuc_hangup_btn').click(function(event) {
        Cti.hangup();
    });
    $('#xuc_login_btn').click(function(event) {
        Cti.loginAgent($('#xuc_agentPhoneNumber').val());
    });
    $('#xuc_logout_btn').click(function(event) {
        Cti.logoutAgent();
    });
    $('#xuc_togglelogin_btn').click(function(event) {
        Cti.toggleAgentLogin();
    });
    $('#xuc_pause_btn').click(function(event) {
        Cti.pauseAgent();
    });
    $('#xuc_unpause_btn').click(function(event) {
        Cti.unpauseAgent();
    });
    $('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
        Cti.subscribeToQueueStats();
    });
    $('#xuc_answer_btn').click(function(event) {
        Cti.answer();
    });
    $('#xuc_hangup_btn').click(function(event) {
        Cti.hangup();
    });
    
   ..............

* Declaring events handlers :

::

    Cti.setHandler(Cti.MessageType.USERSTATUSES, usersStatusesHandler);
    Cti.setHandler(Cti.MessageType.USERSTATUSUPDATE, userStatusHandler);
    Cti.setHandler(Cti.MessageType.LOGGEDON, loggedOnHandler);
    Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, phoneStatusHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, linkStatusHandler);
    Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
    Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
    Cti.setHandler(Cti.MessageType.QUEUEMEMBER, queueMemberHandler);
    Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, directoryResultHandler);

    Cti.setHandler(Cti.MessageType.AGENTCONFIG, agentConfigHandler);
    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, agentStateEventHandler);
    Cti.setHandler(Cti.MessageType.AGENTERROR, agentErrorHandler);
    Cti.setHandler(Cti.MessageType.ERROR, errorHandler);
    Cti.setHandler(Cti.MessageType.AGENTDIRECTORY, agentDirectoryHandler);

Messages
========

Error
-----
* Cti.MessageType.ERROR

LoggedOn
--------
* Cti.MessageType.LOGGEDON

Sheet
-----
* Cti.MessageType.SHEET

User Satuses
------------
* Cti.MessageType.USERSTATUSES : "UsersStatuses"

User Status Update
------------------
* Cti.MessageType.USERSTATUSUPDATE : "UserStatusUpdate",

Directory Result
----------------
* Cti.MessageType.DIRECTORYRESULT

Phone Status Update
-------------------
* Cti.MessageType.PHONESTATUSUPDATE

Link Status Update
------------------
* Cti.MessageType.LINKSTATUSUPDATE

Queue Statistics
----------------
* Handler on :  Cti.MessageType.QUEUESTATISTICS

The handler is executed when a notification of new statistic values is received. Each message contains one or more
counters for one queue. The queue is identified by its queueId. See example below for reference.
The queue's id can be used to retrieve queue's configuration, see `Queue Configuration`_.

Following counters are available:

* TotalNumberCallsEntered
* TotalNumberCallsAnswered
* PercentageAnsweredBefore15
* TotalNumberCallsAbandonned
* TotalNumberCallsAbandonnedAfter15
* PercentageAbandonnedAfter15
* WaitingCalls
* LongestWaitingTime
* EWT
* AvailableAgents
* TalkingAgents

::

    {"queueId":22,"counters":[{"statName":"TalkingAgents","value":15},{"statName":"AvailableAgents","value":12},{"statName":"EWT","value":62}]}

Some messages contain a queueRef with a queue's name instead of the queueId. This issue should be eliminated in
future versions.

::

    {"queueRef":"travels","counters":[{"statName":"TotalNumberCallsAbandonned","value":19}]}

Queue Member
------------
* Handler on : Cti.MessageType.QUEUEMEMBER

Received when an agent is associated to a queue or a penalty is updated. Penalty is -1 when agent is removed from a queue

::

    {"agentId":19,"queueId":3,"penalty":12}

Queue Member List
-----------------
* Handler on : Cti.MessageType.QUEUEMEMBERLIST

Get Queue Statistics
--------------------
* Cti.MessageType.GETQUEUESTATISTICS

Agent State Event
-----------------
* Cti.MessageType.AGENTSTATEEVENT

Agent Error
-----------
* Cti.MessageType.AGENTERROR

Agent Directory
---------------
* Cti.MessageType.AGENTDIRECTORY

Agent Configuration
-------------------
* Cti.MessageType.AGENTCONFIG
* {"id":23,"firstName":"Jack","lastName":"Flash","number":"2501","context":"default"}

Agent List
----------
* Cti.MessageType.AGENTLIST

Receives agent configuration in a javascript Array

Queue Configuration
-------------------
* Cti.MessageType.QUEUECONFIG
* {"id":8,"context":"default","name":"blue","displayName":"blue sky","number":"3506"}

Queue List
-------------------
* Cti.MessageType.QUEUELIST

Methods
=======
Cti.getList(objectType)
-----------------------
Request a list of configuration objects, objectType can be :

* queue
* agent
* queuemember

Triggers handlers QUEUELIST, AGENTLIST, QUEUEMEMBERLIST.
Subscribes to configuration modification changes, handlers QUEUECONFIG, AGENTCONFIG, QUEUEMEMBER can also be called

Cti.setAgentQueue(agentId, queueId, penalty)
----------------------------------------------
* agentId (Integer) : id of agent, returned in message `Agent Configuration`_
* queueId (Integer) : id of queue, returned in message `Queue Configuration`_
* penaly (Integer) : positive integer

If agent is not associated to the queue, associates it, otherwise changes the penalty

On success triggers a `Queue Member`_ event, does not send anything in case of failure :

::

    {"agentId":<agentId>,"queueId":<queueId>,"penalty":<penalty>}

Cti.removeAgentFromQueue(agentId, queueId)
----------------------------------------------
* agentId (Integer) : id of agent, returned in message `Agent Configuration`_
* queueId (Integer) : id of queue, returned in message `Queue Configuration`_

On success triggers a queue member event with penalty equals to -1, does not send anything in case of failure :

::

    {"agentId":<agentId>,"queueId":<queueId>,"penalty":-1}

Cti.subscribeToQueueStats()
---------------------------

This command subscribes to the queue statistics notifications. First, all actual statistics values are sent for
initialisation and then a notification is sent on each update. Both initial values and updates are transmitted
by the QUEUESTATISTICS messages.
