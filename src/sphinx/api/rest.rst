.. restapi:

########
Rest API
########


General form
============

   http://localhost:$xucport/xuc/api/1.0/$method/$domain/$username/
   
   withHeaders(("Content-Type", "application/json"))
   
   * $xucport : Xuc port number (default 9000)
   * $method : See available methods below
   * $domain : Represents a connection site, can be anything
   * $username : XiVO client user username

Events
======

  Xuc post JSON formated events on URL ``eventUrl = "http://localhost:9090/xivo/1.0/event/avencall.com/dropbox/"`` 
  configured in /usr/share/xuc/application.conf

Connection
==========

   POST http://localhost:$xucport/xuc/api/1.0/connect/$domain/$username/

::

   {"password" : "password"}

DND
===

   POST http://localhost:$xucport/xuc/api/1.0/dnd/$domain/$username/

::

   {"state" : [false|true]}


Dial
====
   POST http://localhost:$xucport/xuc/api/1.0/dial/$domain/$username/

::

   {"number" : "1101"}
   
Forward
=======
All forward commands use the above payload::

      {"state" : [true|false],
        "destination" : "1102")

Unconditionnal
--------------
   POST http://localhost:$xucport/xuc/api/1.0/uncForward/$domain/$username/

On No Answer
------------
   POST http://localhost:$xucport/xuc/api/1.0/naForward/$domain/$username/

On Busy
-------
   POST http://localhost:$xucport/xuc/api/1.0/busyForward/$domain/$username/

Handshake
=========
Will repost all events on the configured URL

   POST http://localhost:$xucport/xuc/api/1.0/handshake/$domain/
