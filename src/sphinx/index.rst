########################################
Xuc Xivo Unified Communication Framework
########################################

Xuc is an application suite developed by Avencall_ Group, based on several free existing components
including XiVO_, and our own developments to provide communication services api and application to businesses.
Xuc is build on Play_ using intensively Akka_ and written in Scala_

.. _Avencall: http://www.avencall.com/
.. _XiVO: http://documentation.xivo.io/
.. _Play: http://www.playframework.com/
.. _Akka: http://akka.io/
.. _Scala: http://www.scala-lang.org/


XiVO is `free software`_. Most of its distinctive components, and Xuc as a whole, are distributed
under the *LGPLv3 license*.

.. _free software: http://www.gnu.org/philosophy/free-sw.html

Xuc is providing

* Javascript API
* Web services
* Sample application
* Simple agent application
* Simple unified communication application pratix
* Contact center supervision
* Contact center statistics

Xuc is composed of 3 modules

* The server module
* The core module
* The statistic module.

Table of Contents
=================

.. toctree::
   :maxdepth: 2

   installation/installation
   developers/developers
   api/javascriptapi
   api/rest
   stats/statistics
   ccmanager/ccmanager

Release Notes
=============

2.4.7
-----
* Fix agent paused during a call
* Agent listen
* Monitor Stop / Start
* Supports agent added dynamically in XiVO configuration
* Add xucami plugin with recording status on agentOnCall event
* Dynamically take into account agent added in xivo

2.3.x
-----
* Agent group management in ccmanager
* Metrics integration (http://server:9000/stats/admin/metrics?name=tech)
* New real time statistics

2.3.0
-----
* Fix login agent request with empty parameters
* Supports cti server restart

2.2.0
-----
* Add router stats in logs
* Send error message if agent login request is invalid
* Check username and password before starting websocket
* Fix unable to start when same user is trying to connect at the same time

2.0.8
-----
* new cti commands setAgentQueue, removeAgentFromQueue, getList
* Added ccmanager application

2.0.4
-----
* Play upgraded to 2.3.3, Scala to 2.11.1

1.9.0
-----

* Removed javascript events : AgentLogin, AgentReady, AgentPaused replaced by agent state events.
* Removed javascript toggleLogin function
* Can supervise agents without having all users connected or started
