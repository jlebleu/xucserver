.. highlightlang:: rest

.. _ccmanager:

#########
Ccmanager
#########

==============
Introduction
==============


CCmanager is a web application to manage a contact center


* Display queues
* Display agents / agents status
* Move or add agents in queue / penalty
* Move of add group of agents in queue / penalty
* Action on agents
    * Login / Logout
    * Pause / Unpause
    * Listen [#f1]_


Start the application : http://<xuc:port>/ccmanager

.. rubric:: Footnotes
.. [#f1] Only supervisors which have their own lines can listen to agents, no supported on mobile supervisors, a line has to be affected to supervisors in xivo