.. _installation:

#######################
Xuc server installation
#######################

============
Prerequisite
============

Necessary Compoments
--------------------

Xuc server embed its own web server, and need to be installed on a wheezy debian.

* Install java::

   apt-get install openjdk-7-jre

XiVO Server Configuration
-------------------------

XiVO configuration
^^^^^^^^^^^^^^^^^^

* Create a telephone user with XiVO Client account, 
      login = xuc / password = 0000.
      See /usr/share/xuc/conf/xuc.conf xivocti section for changes in user credentials if needed.
      profile can be any profile

* Create a Webservices user (Configuration/Accès aux services web) with all rights, login = xivows / password = xivows. 
      See /usr/share/xuc/conf/xuc.conf XivoWs section for changes in user credentials if needed.

* Add Asterisk AMI user.
      Add the following section to the /etc/asterisk/manager.conf file (should be adapted to fit your configuration)

::

    [xuc]
    secret = xucpass
    deny = 0.0.0.0/0.0.0.0
    permit = 192.168.0.0/255.255.255.0
    permit = 127.0.0.1/255.255.255.0
    read = system,call,log,verbose,command,agent,user,dtmf
    write = system,call,log,verbose,command,agent,user,dtmf

The configuration update is loaded by running ``manager reload`` command on the Asterisk's CLI.
The xucami module can be disabled in the configuration (xuc.conf), see the parameter enabled in the section xucami.

* Postgresql
   - /etc/postgresql/9.1/main/postgresql.conf Add/Change "listen_addresses" to match your configuration
   - /etc/postgresql/9.1/main/pg_hba.conf Add/Change configuration to match your configuration

::

    ex: "host    asterisk             asterisk             127.0.0.1/32            md5"  to authorize connections on the same server

Apply new configuration::

   xivo-service restart all

Xuc Installation
----------------

* Install Xuc server

From the repository::

   apt-get install xuc

Or you may install generated package builded from the sources::

   dpkg -i xuc_<version>_all.deb

Xuc Configuration
-----------------
You have to restart xuc server after configuration modifications

Changing listening port number or memory size
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 - /etc/init.d/xuc
    Change "PORT" variable, check with netstat first if port is available
    Add JVM_MEM="-mem 64" below port definition
    Add $JVM_MEM in START_CMD variable, ex: "START_CMD="$JVM_MEM -Dconfig.file=..."

XiVO Server
^^^^^^^^^^^

 - /usr/share/xuc/conf/xuc.conf
    Change "xivohost" variable

Dynamic Configuration
^^^^^^^^^^^^^^^^^^^^^

You can overwrite this value :

* NO_DAEMON
    if set xuc will start in interactive mode

* CONFIG_FILE
    path of an alternative configuration file, overrides default config file /usr/share/xuc/conf/xuc.conf

* LOGGER_CONF
    path of specific logger configuration file, overrides default logger /usr/share/xuc/conf/xuc_logger.xml

* PORT
    port number, overrides default value (9000)


=================
Post installation
=================

Enable automatic restart::

   update-rc.d xuc defaults
   
========
Starting
========

Xuc can be started as a service::

   service xuc [start | stop | restart]

=====
Check
=====

tail -f /var/log/xuc/xuc.log
