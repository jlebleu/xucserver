var MockAgentBuilder = function(id,firstName, lastName) {
    this.agent = {};
    this.agent.id = id;
    this.agent.firstName = firstName;
    this.agent.lastName = lastName;
    this.agent.queueMembers = [];
    this.state = {};
    this.state.agentId = id;

    this.inQueue = function(queueId, penalty) {
        this.agent.queueMembers[queueId] = penalty;
        return this;
    };
    this.inGroup = function(groupId) {
        this.agent.groupId = groupId;
        return this;
    };
    this.build = function() {
        return this.agent;
    };
    this.load = function(xucAgent) {
        xucAgent.onAgentConfig(this.agent);
        if (typeof(this.state.name) != 'undefined')
            xucAgent.onAgentState(this.state);
        return xucAgent.getAgent(this.agent.id);
    };
    this.onState = function(agentState) {
        this.state.name = agentState;
        return this;
    };
    this.onPhone = function(phoneNb) {
        this.state.phoneNb = phoneNb;
        return this;
    };
    return this;
};

var MockGroupBuilder = function(id, name) {
    this.group = {};
    this.group.id = id;
    this.group.name = name;

    this.build = function() {
        return this.group;
    };
    return this;
};

var queueId = 1;
var QueueBuilder = function(name, displayName) {
    this.queue = {};
    this.queue.name = name;
    this.queue.displayName = displayName;
    this.queue.id = queueId;
    queueId = queueId + 1;

    this.withDisplayName = function(displayName) {
        this.queue.displayName = displayName;
        return this;
    };
    this.build = function() {
        return this.queue;
    };
    return this;
};