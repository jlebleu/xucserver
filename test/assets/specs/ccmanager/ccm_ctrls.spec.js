describe('ccm controllers', function() {
    var localStorageService;
    var xucQueue;
    var $scope;
    var ctrl;

    beforeEach(module('ccManager'));

    beforeEach(inject(function(_XucQueue_,_localStorageService_, $controller, $rootScope) {
        $scope = $rootScope.$new();
        xucQueue = _XucQueue_;
        localStorageService = _localStorageService_;
        ctrl = $controller('queueSelectCtrl', {
            '$scope' : $scope,
            'XucQueue' : xucQueue,
            'localStorageService' : localStorageService
        });
        spyOn(localStorageService, 'set');
    }));


    it('should save new selection', function() {
        $scope.toggleQueue(3);
        expect(localStorageService.set).toHaveBeenCalledWith('queueSelection',$scope.queueSelection);
    });
});
