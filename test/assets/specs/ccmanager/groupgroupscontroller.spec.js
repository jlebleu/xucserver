describe('ccm groupGroupsController', function() {
    var xucGroup;
    var $scope;
    var $rootScope;

    beforeEach(module('ccManager'));


    beforeEach(inject(function(_XucGroup_, $controller, _$rootScope_) {
        $rootScope =_$rootScope_;
        $scope = $rootScope.$new();
        xucGroup = _XucGroup_;
        ctrl = $controller('groupGroupsController', {
            '$scope' : $scope,
            'XucGroup' : xucGroup
        });

    }));

    it('should be able to move group of agents from a queue/penalty to an other on drag and drop', function(){
        spyOn(xucGroup,'moveAgentsInGroup');

        $scope.agentInGroup(2,1,4,32,10,false);

        expect(xucGroup.moveAgentsInGroup).toHaveBeenCalledWith(2,1,4,32,10);

    });

    it('should be able to add group of agents to a queue/penalty', function() {
        var groupId = 8, fromQueueId =741, fromPenalty =3;
        var toQueueId = 945, toPenalty = 9;

        spyOn(xucGroup,'addAgentsInGroup');

        $scope.agentInGroup(groupId,fromQueueId, fromPenalty, toQueueId, toPenalty, true);

        expect(xucGroup.addAgentsInGroup).toHaveBeenCalledWith(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
    });

    it('should be able to remove a group from a queue/penalty', function(){
        var groupId=27, queueId = 234, penalty = 1;

        spyOn(xucGroup,'removeAgentGroupFromQueueGroup');

        $scope.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);

        expect(xucGroup.removeAgentGroupFromQueueGroup).toHaveBeenCalledWith(groupId, queueId, penalty);
    });

});