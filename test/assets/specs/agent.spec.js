'use strict';
describe('Agent Call Controller', function() {
    var $scope, ctrl;

    beforeEach(module('xuc.services'));
    beforeEach(module('Agent'));

    beforeEach(inject(function($rootScope, $controller, $timeout) {
        $scope = $rootScope.$new();
        ctrl = $controller('CallController', {
            '$scope' : $scope,
            $timeout : $timeout
        });
    }));

    it('should popup when url and param are in sheet event ', function() {
        $scope.openWindow = function(url) {
        }
        spyOn($scope, 'openWindow');

        var fields = {};
        fields["popupUrl"] = "http://localhost/";
        fields["folderNumber"] = "23678";

        $scope.popUp(fields);

        expect($scope.openWindow).toHaveBeenCalledWith("http://localhost/23678");
    });

    it('should not popup when url not defined ', function() {
        var opencalled = false;
        $scope.openWindow = function(url) {
            opencalled = true;
        }

        var fields = {};
        fields["folderNumber"] = "23678";

        $scope.popUp(fields);

        expect(opencalled).toBe(false);
    });
    it('should not popup when url is empty ', function() {
        var opencalled = false;
        $scope.openWindow = function(url) {
            opencalled = true;
        }

        var fields = {};
        fields["folderNumber"] = "23678";
        fields["popupUrl"] = "";

        $scope.popUp(fields);

        expect(opencalled).toBe(false);
    });
    it('should not popup when folderNumber not defined ', function() {
        var opencalled = false;
        $scope.openWindow = function(url) {
            opencalled = true;
        }

        var fields = {};
        fields["popupUrl"] = "http://localhost/";

        $scope.popUp(fields);

        expect(opencalled).toBe(false);
    });
    it('should not popup when folder number is empty ', function() {
        var opencalled = false;
        $scope.openWindow = function(url) {
            opencalled = true;
        }

        var fields = {};
        fields["folderNumber"] = "";
        fields["popupUrl"] = "http://localhost/";

        $scope.popUp(fields);

        expect(opencalled).toBe(false);
    });
    it('should not popup when folder number has default value ', function() {
        var opencalled = false;
        $scope.openWindow = function(url) {
            opencalled = true;
        }

        var fields = {};
        fields["folderNumber"] = "-";
        fields["popupUrl"] = "http://localhost/";

        $scope.popUp(fields);

        expect(opencalled).toBe(false);
    });

});