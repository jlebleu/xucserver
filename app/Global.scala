import play.api.Application
import play.api.GlobalSettings
import play.api.Logger

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    Logger.info("Starting xuc")
  }

  override def onStop(app: Application) {
    Logger.debug("Shutting down actor system")
  }
}
