package controllers

import controllers.helpers.PrettyController
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms._
import play.api.i18n.Lang
import play.api.mvc.{Action, Controller}
import play.api.data.Forms.text
import models.XucUser

object CcManager extends Controller {

  val log = Logger(getClass.getName)

  val Home = Redirect(routes.CcManager.connect)

  val lang = Lang("fr")

  def index = Action { Home }

  def connect = Action {
    log.debug("Returning login page")
    Ok(PrettyController.prettify(views.html.ccmanager.login(loginForm)(lang)))
  }

  def loginError(error: String) = Action {
    log.info("Back to login page with error: " + error)
    val loginFormWithErrors = loginForm.withGlobalError(error)
    BadRequest(PrettyController.prettify(views.html.ccmanager.login(loginFormWithErrors)(lang)))
  }

  def login = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => {
        log.error("Login form error, returning for corrections: " + formWithErrors.toString)
        BadRequest(PrettyController.prettify(views.html.ccmanager.login(formWithErrors)(lang)))
      },
      user => {
        log.info("Logging to the cti control panel page with username: " + user.ctiUsername)
        Ok(PrettyController.prettify(views.html.ccmanager.ccmanager("Xivo CC Manager", user, lang)))
      })
  }
  val loginForm = Form(
    mapping(
      "ctiUsername" -> nonEmptyText,
      "ctiPassword" -> text(minLength = 3),
      "phoneNumber" -> optional(number),
      "userId" -> optional(text),
      "agentId" -> optional(text),
      "phoneId" -> optional(text)
    )(XucUser.apply)(XucUser.unapply)
  )
}