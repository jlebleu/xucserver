package controllers

import controllers.helpers.PrettyController
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.text
import play.api.data.Forms.tuple
import play.api.i18n.Lang
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.Routes

object AvCcSupervision extends Controller {

  val lang = Lang("fr")

  val IndexFa = Redirect(routes.AvCcSupervision.indexfa)
  
  def index = Action { IndexFa }

  def indexfa = Action { implicit request =>
    Ok(PrettyController.prettify(views.html.ccsupervision.supervisionfa("Contact Center Supervision", lang)))
  }

  def indexaf = Action { implicit request =>
    Ok(PrettyController.prettify(views.html.ccsupervision.supervisionaf("Contact Center Supervision", lang)))
  }

}