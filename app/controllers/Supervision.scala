package controllers

import controllers.helpers.PrettyController
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms.nonEmptyText
import play.api.data.Forms.text
import play.api.data.Forms.tuple
import play.api.i18n.Lang
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.Routes

object Supervision extends Controller {

  val lang = Lang("fr")

  def index = Action { implicit request =>
    Ok(PrettyController.prettify(views.html.sample.supervision("XiVO Web Supervision")))
  }

}