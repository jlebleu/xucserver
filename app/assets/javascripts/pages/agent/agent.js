var agentModule = angular.module('Agent', ['xuc.services']);

agentModule.controller('ctiLinkController', function($rootScope, $scope, $timeout) {
 
    $scope.reconnectTimeout = 20000;

    $scope.loggedOnHandler = function(event) {
        console.log("logged on to cti server");
        $rootScope.$broadcast('ctiLoggedOn');
    };

    $scope.linkStatusHandler = function(event) {
        console.log("link status : " + event.status);
        $scope.status = event.status;
        if (event.status !== 'opened') {
            $rootScope.$broadcast('linkDisConnected');
            window.location.replace("/agent?error='unable to connect to xuc server'");
        }
        $scope.$apply();
    };

    Cti.debugMsg = false;
    DirectoryDisplay.init("#directoryresult");
    Cti.setHandler(Cti.MessageType.LOGGEDON, $scope.loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, $scope.linkStatusHandler);
});

agentModule.controller('AgentButtonController', function($rootScope, $scope, $timeout) {

    $scope.loginButton = {};
    $scope.pauseButton = {};
    $scope.pauseButton.show = "disabled";
    $scope.agentStatus = {};
    $scope.agentStatus.label = "";
    $scope.agentStatus.start = moment();
    $scope.agentStatus.moment = "0 second";
    $rootScope.agent = {};
    $rootScope.agent.queueMembers = {};
    $scope.loggedIn = false;

    $scope.loginHandler = function(event) {
        $scope.loggedIn = true;
        $scope.loginButton.label = "Delogguer l'agent";
        $scope.loginButton.color = "btn-warning";
        $scope.agentStatus.label = "logged in";
        $scope.agentStatus.start = moment();
        $scope.agentStatus.moment = "0 second";
        $scope.pauseButton.show = "";
        $scope.$apply();
    };
    $scope.logoutHandler = function() {
        $scope.loggedIn = false;
        $scope.loginButton.label = "Logguer l'agent";
        $scope.loginButton.color = "btn-success";
        $scope.agentStatus.label = "logged out";
        $scope.agentStatus.start = moment();
        $scope.agentStatus.moment = "0 second";
        $scope.pauseButton.label = "";
        $scope.pauseButton.show = "disabled";
        $scope.$apply();
    };

    $scope.toggleLogin = function() {
        if ($scope.loggedIn)
            Cti.logoutAgent();
        else
            Cti.loginAgent(Cti.agentNumber);
    };

    $scope.pausedHandler = function() {
        $scope.pauseButton.label = "Passer prêt";
        $scope.pauseButton.show = "";
        $scope.agentStatus.label = "Not Ready";
        $scope.agentStatus.start = moment();
        $scope.agentStatus.moment = "0 second";
        $scope.pause = false;
        $scope.$apply();
    };
    $scope.readyHandler = function() {
        $scope.pauseButton.label = "Se mettre en Pause";
        $scope.pauseButton.show = "";
        $scope.agentStatus.label = "Ready";
        $scope.agentStatus.start = moment();
        $scope.agentStatus.moment = "0 second";
        $scope.pause = true;
        $scope.$apply();
    };
    $scope.togglePause = function() {
        if ($scope.pause)
            Cti.pauseAgent();
        else
            Cti.unpauseAgent();
    };
    $scope.onAgentStateEvent = function(event) {
        $scope.phoneNb = event.phoneNb;
        $rootScope.agent.phoneNb = event.phoneNb;
        $rootScope.agent.id = event.agentId;

        switch (event.name) {
        case "AgentOnPause":
            $scope.pausedHandler();
            $scope.loginHandler();
            break;
        case "AgentReady":
            $scope.readyHandler();
            $scope.loginHandler();
            break;
        case "AgentOnCall":
            $scope.loginHandler();
            break;
        case "AgentLoggedOut":
            $scope.logoutHandler();
            break;
        }
    };

    $scope.onTimeout = function() {
        $scope.agentStatus.moment = moment().countdown($scope.agentStatus.start).toString();
        mytimeout = $timeout($scope.onTimeout, 5000,false);
    };
    var mytimeout = $timeout($scope.onTimeout, 5000,false);

    $scope.stop = function() {
        $timeout.cancel(mytimeout);
    };
    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, $scope.onAgentStateEvent);

});
agentModule.controller('QueueController',
    ['XucQueue','$scope','$timeout','$rootScope',
    function(XucQueue,$scope,$timeout,$rootScope) {

    var queueRefreshTimeout = 5000;

    loadQueueMembers = function() {
        Cti.getConfig("queuemember");
    };

    $scope.agentqueues = function(queue) {
        return (typeof $rootScope.agent.queueMembers[queue.id] !== 'undefined');
    };

    $scope.$on('ctiLoggedOn', function(e) {
        XucQueue.start();
        console.log("cti logged on");
        $timeout(loadQueueMembers,5000);
    });

    onQueueMember = function(queueMember) {
        if (queueMember.agentId === $rootScope.agent.id) {
            if (queueMember.penalty >= 0) {
                $rootScope.agent.queueMembers[queueMember.queueId] = {};
                $rootScope.agent.queueMembers[queueMember.queueId] = queueMember.penalty;
            } else {
                delete $rootScope.agent.queueMembers[queueMember.queueId];
            }
        }
    };

    $scope.getQueues = function() {
        XucQueue.updateQueues(queueRefreshTimeout/1000);
        $scope.queues = XucQueue.getQueues();
        $timeout($scope.getQueues,queueRefreshTimeout,false);
        $scope.$digest();
    };
    $timeout($scope.getQueues,queueRefreshTimeout,false);
    Cti.setHandler(Cti.MessageType.QUEUEMEMBER, onQueueMember);
}]);

agentModule.controller('PhoneController', function($rootScope, $scope, $timeout) {
    $scope.phoneStatus = {};
    $scope.phoneStatus.label = "";
    $scope.phoneStatus.start = moment();
    $scope.phoneStatus.moment = "0 second";

    $scope.normalize = function(nb) {
        nb = "" + nb;
        return nb.replace(/[ ()\.]/g, "");
    };
    $scope.phoneStatusHandler = function(event) {
        $scope.phoneStatus.label = event.status;
        $scope.phoneStatus.start = moment();
        if (event.status == "AVAILABLE")
            $rootScope.$broadcast('phone_available');
        $scope.$apply();
    };
    $scope.updatePhoneStatus = function() {
        $scope.phoneStatus.moment = moment().countdown($scope.phoneStatus.start).toString();
        $scope.$digest();
        mytimeout = $timeout($scope.updatePhoneStatus, 5000, false);
    };

    $scope.stop = function() {
        $timeout.cancel(mytimeout);
    };

    $scope.dial = function() {
        Cti.originate($scope.normalize($scope.destination));
    };
    $scope.attendedTransfer = function() {
        Cti.attendedTransfer($scope.normalize($scope.destination));
    };
    $scope.completeTransfer = function() {
        Cti.completeTransfer();
    };
    $scope.cancelTransfer = function() {
        Cti.cancelTransfer();
    };
    $scope.answer = function() {
        Cti.answer();
    };
    $scope.hangup = function() {
        Cti.hangup();
    };
    $scope.conference = function() {
        Cti.conference();
    };
    $scope.hold = function() {
        Cti.hold();
    };
    var mytimeout = $timeout($scope.updatePhoneStatus, 5000, false);
    Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, $scope.phoneStatusHandler);
});
agentModule.controller('CallController', function($scope, $timeout) {
    $scope.show = "hidden";
    $scope.fields = [];

    $scope.testPopup = function() {
        console.log("testing popup");
        var sh = {};
        sh.payload = {};
        sh.payload.profile = {};
        sh.payload.profile.user = {};
        sh.payload.profile.user.sheetInfo = {};
        sh.payload.profile.user.sheetInfo[1] = {
            'name' : 'test',
            'value' : 'this is a test'
        };
        sh.payload.profile.user.sheetInfo[2] = {
            'name' : 'folderNumber',
            'value' : ''
        };
        sh.payload.profile.user.sheetInfo[3] = {
            'name' : 'popupUrl',
            'value' : 'http://localhost/'
        };
        $scope.receiveSheet(sh);
    };
    $scope.receiveSheet = function(sheet) {
        $scope.fields = sheet.payload.profile.user.sheetInfo;
        $scope.show = "show";
        $scope.$apply();
        var fields = $scope.decodePayload(sheet.payload);
        $scope.popUp(fields);
    };

    $scope.popUp = function(fields) {
        var folderNumber = fields.folderNumber;
        var popUrl = fields.popupUrl;
        if ($scope._fieldIsDefined(popUrl)) {
            popUrl = popUrl + folderNumber;
            if ($scope._fieldIsDefined(folderNumber) && folderNumber !== "-") {
                console.log("opening : " + popUrl);
                $scope.openWindow(popUrl);
            }
        }
    };
    $scope._fieldIsDefined = function(field) {
        return (typeof field != 'undefined' && field !== "");
    };

    $scope.openWindow = function(popUrl) {
        window.open(popUrl);
    };
    $scope.decodePayload = function(payload) {
        var sheetInfo = payload.profile.user.sheetInfo;
        var sheetFields = {};
        for ( var i in sheetInfo) {
            sheetFields[sheetInfo[i].name] = sheetInfo[i].value;
        }
        return sheetFields;
    };
    $scope.$on('phone_available', function(e) {
        $scope.show = "hidden";
        $scope.fields = [];
    });
    Cti.setHandler(Cti.MessageType.SHEET, $scope.receiveSheet);
});

$("#numberTodial").keypress(function(event) {
    var normalize = function(nb) {
        nb = "" + nb;
        return nb.replace(/[ ()\.]/g, "");
    };
    if (event.which == $.ui.keyCode.ENTER) {
        event.preventDefault();
        var toBeDialed = normalize($("#numberTodial").val());
        if ($.isNumeric(toBeDialed)) {
            Cti.dial(toBeDialed);
        } else {
            Cti.searchDirectory(toBeDialed);
        }
    }
});
