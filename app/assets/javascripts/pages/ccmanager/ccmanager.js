var ccManagerApp = angular.module('ccManager', ['xuc.services','ngCookies','ui.bootstrap','LocalStorageModule','ngTable'])
                    .config(['localStorageServiceProvider', function(localStorageServiceProvider){
                        localStorageServiceProvider.setPrefix('ccManager.default');
                        localStorageServiceProvider.setNotify(true,true);
                    }]);



ccManagerApp.controller('ccQueuesController',
    ['XucQueue','XucAgent','XucGroup', '$scope', '$timeout', 'localStorageService',
    function(xucQueue, xucAgent, xucGroup, $scope, $timeout, localStorageService) {

    var MAXPENALTY = 20;

    $scope.queues = [];
    $scope.ratioAffichage = 1;
    $scope.groupsArray = [];
    $scope.groupGroups = [];
    $scope.folded = false;
    $scope.groupView = false;

    $scope.isFolded = function(penalty) {
        return (penalty === 0) && $scope.folded;
    };
    $scope.isUnFolded = function(penalty) {
        return (penalty === 0) && !$scope.folded;
    };
    $scope.fold = function() {
        $scope.folded = true;
    };
    $scope.unFold = function() {
        $scope.folded = false;
    };
    $scope.showGroup = function(penalty) {
        return ((penalty === 0) || !$scope.folded );
    };
    $scope.setGroupView =function() {
        $scope.groupView = true;
    };
    $scope.setAgentView =function() {
        $scope.groupView = false;
    };
    $scope.$on('ctiLoggedOn', function(e) {
        xucQueue.start();
        xucGroup.start();
    });

    $scope.$on('linkDisConnected', function(e) {
        $scope.connected = false;
        $scope.queues.length = 0;
    });

    var agentGroupTpl = function(penalty) { return( {'penalty' : penalty, 'agents' : []});};

    var initGroupGbl = function(template, groups) {
        var initGrp = function(penalty) {
            if (typeof groups[penalty] === 'undefined') groups[penalty] = template(penalty);
            if (!groups[penalty-1] && penalty > 0) initGrp(penalty-1);

        };
        return initGrp;
    };

    var addToAgentGroup = function(group, agent) {
        group.agents.push(agent);
    };
    $scope.buildGroups = function(queueId, agents, addToGroup, template) {
        var groups = [];
        var initGroup = initGroupGbl(template, groups);
        var getGroup = function(penalty) {
            if (typeof groups[penalty] === 'undefined') initGroup(penalty);
            return groups[penalty];
        };
        var buildGroups = function() {
            angular.forEach(agents, function (agent, keyAgent) {
                if (typeof agent.queueMembers[queueId] !== 'undefined') {
                    var group = getGroup(agent.queueMembers[queueId]);
                    addToGroup(group,agent);
                    if (agent.queueMembers[queueId]+1 < MAXPENALTY) initGroup(agent.queueMembers[queueId]+1);
                }
            });
        };
        initGroup(0);
        buildGroups();
        return groups;
    };
    $scope.buildGroupMember = function(queueId, agents) {
        var groups = $scope.buildGroups(queueId,agents,addToAgentGroup, agentGroupTpl);
        $scope.groupsArray[queueId] = {'idQueue' : queueId, 'groups' : groups};
    };
    $scope.buildGroupOfGroups = function(queueId, agents) {
        var groups = xucGroup.getGroupsForAQueue(queueId);
        $scope.groupGroups[queueId] = {'idQueue' : queueId, 'groups' : groups};
    };

    var initQueueGroupMembers = function() {
        for (var i = 0; i < $scope.queues.length; i++) {
            $scope.buildGroupMember($scope.queues[i].id,[]);
            $scope.buildGroupOfGroups($scope.queues[i].id,[]);
        }
    };
    var selectQueue = function(queueSelection) {
        var isQueueSelected = function(queue) {
            return queueSelection[queue.id];
        };
        $scope.queues = xucQueue.getQueues().filter(isQueueSelected);
    };

    $scope.$on('QueuesLoaded', function() {
        var queueSelection = localStorageService.get('queueSelection') || {};
        selectQueue(queueSelection);
        initQueueGroupMembers();
    });

    $scope.$on('LocalStorageModule.notification.setitem', function(event,args) {
        if (args.key === 'queueSelection') {
            var queueSelection = localStorageService.get('queueSelection') || {};
            selectQueue(queueSelection);
        }
    });

    $scope.$on('QueueMemberUpdated', function(event, queueId){
        var ags = xucAgent.getAgentsInQueue(queueId);
        $scope.buildGroupMember(queueId,ags);
        $scope.buildGroupOfGroups(queueId,ags);
        $scope.$digest();
    });
}]);
ccManagerApp.controller('groupGroupsController',
    ['$scope','XucGroup', function($scope, xucGroup) {

    $scope.agentInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty, clone) {
        if (clone) {
            xucGroup.addAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
        }
        else {
            xucGroup.moveAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
        }
    };
    $scope.removeAgentGroupFromQueueGroup = function(groupId, queueId, penalty) {
        xucGroup.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);
    };
}]);

ccManagerApp.controller('queueSelectCtrl',
    ['$scope','XucQueue','localStorageService', function($scope, XucQueue, localStorageService) {
    $scope.queueSelection = localStorageService.get('queueSelection') || {};
    $scope.queues = XucQueue.getQueues();

    var noQueueSelected = function() {
        var selected = false;
        angular.forEach($scope.queueSelection, function(value, key) {
          selected = selected || value;
        });
        return !selected;
    };
    $scope.isSelected = function(queueId) {
        return $scope.queueSelection[queueId];
    };
    $scope.toggleQueue = function(queueId) {
        $scope.queueSelection[queueId] = !$scope.queueSelection[queueId];
        localStorageService.set('queueSelection', $scope.queueSelection);
    };
    $scope.noQueueLoaded = function() {
        return $scope.queues.length === 0;
    };
    $scope.noQueueSelected = function() {
        return $scope.queueSelection.length === 0;
    };
    $scope.$on('QueuesLoaded', function() {
        $scope.queues = XucQueue.getQueues();
        $scope.status.disabled = ($scope.queues.length === 0);
        $scope.status.opened = noQueueSelected() && ($scope.queues.length > 0);
    });

    $scope.status = {
        disabled: ($scope.queues.length === 0),
        opened: ($scope.queueSelection.length === 0)
    };
}]);
ccManagerApp.controller('addAgentGroupCtrl',
    ['$scope','XucGroup','$modal','ngTableParams','$filter',
    function($scope, xucGroup, $modal, ngTableParams, $filter) {

    var ModalInstanceCtrl = function ($scope, $modalInstance,
            ngTableParameters, xucGroup, queue, penalty) {

      $scope.groups = xucGroup.getAvailableGroups(queue.id, penalty);
      $scope.queue = queue;
      $scope.penalty = penalty;

      $scope.addGroup = function(group, queueId, penalty) {
        xucGroup.addAgentsNotInQueueFromGroupTo(group.id,queueId, penalty);
        $modalInstance.dismiss('done');
      };
      $scope.tableSettings = function() {
        var calculatePage = function(params) {
            if (params.total() <= (params.page() - 1)  * params.count()) {
                var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
                params.page(setPage);
            }
        };
        return {
            total: $scope.groups.length, // length of data
            counts:[],
            getData: function($defer, params) {
                var orderedData = params.sorting() ?
                                    $filter('orderBy')($scope.groups, params.orderBy()) :
                                    $scope.groups;
                params.total(orderedData.length);
                calculatePage(params);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        };
      };

      $scope.agentGroups = new ngTableParameters({
        page: 1,
        count: 10,
        sorting: {
            name: 'asc'
        }
      }, $scope.tableSettings()
      );

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    $scope.addAgentGroup = function(queue, penalty) {
        $scope.queue = queue;
        $scope.penalty = penalty;

        var modalInstance = $modal.open({
          templateUrl: 'addAgentGroupContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            ngTableParameters :function () {
              return ngTableParams;
            },
            xucGroup: function() {
                return xucGroup;
            },
            queue: function() {
                return $scope.queue;
            },
            penalty: function() {
                return $scope.penalty;
            }
          }
        });

        modalInstance.result.then(function (agent) {
        }, function () {
             console.log('Modal dismissed at: ' + new Date());
            }
        );
    };
}]);

ccManagerApp.controller('virtualGroupController',
    ['$scope','$modal','ngTableParams','$filter',
    function($scope, $modal, ngTableParams, $filter) {

    var ModalInstanceCtrl = function ($scope, $modalInstance,
            ngTableParameters, group, queue, penalty) {

      $scope.group = group;
      $scope.agents = angular.copy(group.agents);
      $scope.queue = queue;
      $scope.penalty = penalty;

      $scope.tableSettings = function() {
        var calculatePage = function(params) {
            if (params.total() <= (params.page() - 1)  * params.count()) {
                var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
                params.page(setPage);
            }
        };
        return {
            total: $scope.agents.length, // length of data
            counts:[],
            getData: function($defer, params) {
                var orderedData = params.sorting() ?
                                    $filter('orderBy')($scope.agents, params.orderBy()) :
                                    $scope.agents;
                params.total(orderedData.length);
                calculatePage(params);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        };
      };

      $scope.agentsInGroup = new ngTableParameters({
        page: 1,
        count: 10,
        sorting: {
            name: 'asc'
        }
      }, $scope.tableSettings()
      );

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    $scope.showGroup = function(group, queue, penalty) {
        $scope.group = group;

        var modalInstance = $modal.open({
          templateUrl: 'showGroupContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            ngTableParameters :function () {
              return ngTableParams;
            },
            group: function() {
                return $scope.group;
            },
            queue: function() {
                return $scope.queue;
            },
            penalty: function() {
                return penalty;
            }
          }
        });

        modalInstance.result.then(function (agent) {
        }, function () {
             console.log('Modal dismissed at: ' + new Date());
            }
        );
    };
}]);
ccManagerApp.controller('ccChooseAgentCtrl',
    ['$scope','XucAgent','$modal', function($scope, XucAgent, $modal) {

    var ModalInstanceCtrl = function ($scope, $modalInstance, agents) {

      $scope.agents = agents;
      $scope.orderList = "lastName";
      $scope.selected = {
        agent: $scope.agents[0]
      };

      $scope.ok = function () {
        $modalInstance.close($scope.selected.agent);
      };
      $scope.selected = function (agent) {
        $modalInstance.close(agent);
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    };

    $scope.agents = XucAgent.getAgents();
    $scope.addAgent = function(queueId,penalty) {
        $scope.agents = XucAgent.getAgentsNotInQueue(queueId);
        var modalInstance = $modal.open({
          templateUrl: 'myModalContent.html',
          controller: ModalInstanceCtrl,
          size: 'sm',
          resolve: {
            agents: function () {
              return $scope.agents;
            }
          }
        });
        modalInstance.result.then(function (agent) {
             Cti.setAgentQueue(agent.id,queueId,penalty);
        }, function () {
             console.log('Modal dismissed at: ' + new Date());
            }
        );
    };
}]);
ccManagerApp.controller('agentEditController',
    ['$scope','XucAgent','XucQueue','$modal','ngTableParams','$filter',
    function($scope, xucAgent, xucQueue, $modal, ngTableParams,$filter) {

    $scope.loadAgentQueues = function(queueMembers) {
        var queues = [];
        angular.forEach(queueMembers, function (penalty, queueId) {
            var queueMember = xucQueue.getQueue(queueId);
            queueMember.penalty = penalty;
            queues.push(xucQueue.getQueue(queueId));
        });
        return queues;
    };



    var ModalInstanceCtrl = function ($scope, $modalInstance, agent,
            ngTableParameters, xucQueue, xucAgent) {

      $scope.agent = agent;
      $scope.queues = agent.queues;
      $scope.newQueue = {};
      $scope.newQueue.queue = null;
      $scope.newQueue.penalty = 0;
      $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);

      $scope.tableSettings = function() {
        var calculatePage = function(params) {
            if (params.total() <= (params.page() - 1)  * params.count()) {
                var setPage = (params.page()-1) > 0 && (params.page()-1) || 1;
                params.page(setPage);
            }
        };
        return {
            total: $scope.queues.length, // length of data
            counts:[],
            getData: function($defer, params) {
                var orderedData = params.sorting() ?
                                    $filter('orderBy')($scope.queues, params.orderBy()) :
                                    $scope.queues;
                params.total(orderedData.length);
                calculatePage(params);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        };
      };

      $scope.agentQueues = new ngTableParameters({
        page: 1,
        count: 10,
        sorting: {
            penalty: 'asc'
        }
      }, $scope.tableSettings()
      );

      $scope.addQueue = function() {
         var queue = $scope.newQueue.queue;
         queue.penalty = $scope.newQueue.penalty;
         $scope.queues.push(queue);
         $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);
         $scope.newQueue.queue = null;
         $scope.agentQueues.reload();
      };
      $scope.removeQueue = function(queue) {
        $scope.queues.splice($scope.queues.indexOf(queue), 1);
        $scope.availableQueues = xucQueue.getQueuesExcept($scope.queues);
        $scope.agentQueues.reload();
      };
      $scope.ok = function () {
        xucAgent.updateQueues($scope.agent.id,$scope.queues);
        $modalInstance.close($scope.agent);
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
      $scope.validatePenalty = function(queue) {
        if (typeof queue.penalty === 'undefined') {
            queue.penalty = $scope.agent.queueMembers[queue.id] || 0;
        }
      };
    };

    $scope.editAgent = function(agentId) {

        $scope.agent = xucAgent.getAgent(agentId);
        $scope.agent.queues = {};
        $scope.agent.queues = $scope.loadAgentQueues($scope.agent.queueMembers);

        var modalInstance = $modal.open({
          templateUrl: 'agentEditContent.html',
          controller: ModalInstanceCtrl,
          resolve: {
            agent: function () {
              return $scope.agent;
            },
            ngTableParameters :function () {
              return ngTableParams;
            },
            xucQueue: function() {
                return xucQueue;
            },
            xucAgent: function() {
                return xucAgent;
            }
          }
        });

        modalInstance.result.then(function (agent) {
        }, function () {
             console.log('Modal dismissed at: ' + new Date());
            }
        );
    };

}]);
ccManagerApp.controller('ccAgentsController',
    ['$scope', '$timeout', '$filter', '$rootScope','XucAgent',
    function($scope, $timeout, $filter, $rootScope, xucAgent) {

    $scope.agents = [];
    $scope.agentStateTimer = 2;
    $scope.initQueues = true;
    $scope.userStatuses = {};


    $scope.removeAgentsNotDisplayed = function() {
        return $filter('filter')($scope.agents, {
            displayed : true
        });
    };

    $scope.$on('ctiLoggedOn', function(e) {
        console.log("----------------------connected-----------------------");
        $timeout(xucAgent.start, 2000);
    });
    $scope.$on('linkDisConnected', function(e) {
        $scope.connected = false;
        $scope.agents.length = 0;
    });

    $scope.$on('dropAgent', function(event,args) {
        Cti.setAgentQueue(args.idAgent,args.newIdQueue,args.penalty);
        if (! args.clone && (args.newIdQueue !== args.oldIdQueue)) {
            Cti.removeAgentFromQueue(args.idAgent, args.oldIdQueue);
        }
    });
    $scope.$on('dropAgentTrash', function(event,args) {
        Cti.removeAgentFromQueue(args.agentId, args.queueId);
    });

}]);
ccManagerApp.controller('agentActionController',
    ['$scope','XucAgent',
    function($scope, xucAgent) {

    $scope.canLogIn = function(agentId) {
        return xucAgent.canLogIn(agentId);
    };
    $scope.login = function(agentId) {
        xucAgent.login(agentId);
    };
    $scope.canLogOut = function(agentId) {
        return xucAgent.canLogOut(agentId);
    };
    $scope.logout = function(agentId) {
        xucAgent.logout(agentId);
    };
    $scope.canPause = function(agentId){
        return xucAgent.canPause(agentId);
    };
    $scope.pause = function(agentId) {
        xucAgent.pause(agentId);
    };
    $scope.canUnPause = function(agentId){
        return xucAgent.canUnPause(agentId);
    };
    $scope.unpause = function(agentId) {
        xucAgent.unpause(agentId);
    };
    $scope.canListen = function(agentId) {
        return xucAgent.canListen(agentId);
    };
    $scope.listen = function(agentId) {
        xucAgent.listen(agentId);
    };
}]);

ccManagerApp.controller('ctiLinkController', function($rootScope, $scope, $timeout) {
    var clockFrequency = 5;

    $scope.loggedOnHandler = function(event) {
        $rootScope.$broadcast('ctiLoggedOn');
    };

    $scope.linkStatusHandler = function(event) {
        console.log("link status : " + event.status);
        $scope.status = event.status;
        if (event.status !== 'opened') {
            $rootScope.$broadcast('linkDisConnected');
            window.location.replace("/ccmanager?error='unable to connect to xuc server'");
        }
        $scope.$apply();
    };

    // timeout
    var updateClock = function() {
        $scope.clock = moment().format('H:mm:ss');
        mytimeout = $timeout(updateClock, clockFrequency * 1000);
        $scope.$digest();
    };
    $timeout(updateClock, 1000,false);

    Cti.setHandler(Cti.MessageType.LOGGEDON, $scope.loggedOnHandler);
    Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, $scope.linkStatusHandler);
});

var supervisionUsername = "";
var supervisionPassword = "";
var initCti = function(username, password) {
    supervisionUsername = username;
    supervisionPassword = password;
    console.log("sign in " + supervisionUsername + " " + password);
    var wsurl = xucRoutes.controllers.xuc.WebSocketApp.ctiChannel(supervisionUsername, 0, password).webSocketURL();
    Cti.debugMsg = false;
    Cti.WebSocket.init(wsurl, supervisionUsername, 0);
};
