angular.module('ccManager').directive('draggableAgent', function() {
    return {
        replace:true,
        link : function(scope, element) {
            var el = element[0];
            el.draggable = true;
            el.addEventListener(
                'dragstart',
                function(e) {
                    e.dataTransfer.effectAllowed = 'move';
                    e.dataTransfer.setData('idAgent', scope.agent.id);
                    e.dataTransfer.setData('idQueue', scope.queue.id);
                    e.dataTransfer.setData('clone', e.ctrlKey);
                    this.style.opacity = '0.7';
                    return false;
                },
                false
            );
            el.addEventListener(
                'dragend',
                function(e) {
                    this.style.opacity = '1';
                    return false;
                },
                false
            );
        }
    };
}).directive('droppableAgent', function($rootScope) {
    return {
        replace:true,
        link: function(scope, element) {
            var el = element[0];
            el.addEventListener(
                'dragover',
                function(e) {
                    e.dataTransfer.dropEffect = 'move';
                    if (e.preventDefault) e.preventDefault();
                    this.style.backgroundColor = "Orange";
                    return false;
                },
                false
            );
            el.addEventListener(
                'dragleave',
                function(e) {
                    e.dataTransfer.dropEffect = 'move';
                    // allows us to drop
                    if (e.preventDefault) e.preventDefault();
                    this.style.backgroundColor = "";
                    return false;
                },
                false
            );
            el.addEventListener(
                'drop',
                function(e) {
                    if (e.stopPropagation) e.stopPropagation();
                    this.style.backgroundColor = "";
                    $rootScope.$broadcast('dropAgent', {'idAgent' : Number(e.dataTransfer.getData('idAgent')), 'oldIdQueue' : Number(e.dataTransfer.getData('idQueue')),
                                            'newIdQueue' : scope.queue.id, 'penalty' : scope.group.penalty, 'clone' : e.dataTransfer.getData('clone') === 'true'});
                    return false;
                },
                false
            );
          }
      };
}).directive('droppableAgentTrash', function($rootScope) {
      return {
          replace:true,
          link: function(scope, element) {
              var el = element[0];
              el.addEventListener(
                  'dragover',
                  function(e) {
                      e.dataTransfer.dropEffect = 'move';
                      if (e.preventDefault) e.preventDefault();
                      this.style.backgroundColor = "Red";
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'dragleave',
                  function(e) {
                      e.dataTransfer.dropEffect = 'move';
                      // allows us to drop
                      if (e.preventDefault) e.preventDefault();
                      this.style.backgroundColor = "";
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'drop',
                  function(e) {
                      if (e.stopPropagation) e.stopPropagation();
                      this.style.backgroundColor = "";
                      $rootScope.$broadcast('dropAgentTrash', {'agentId' : Number(e.dataTransfer.getData('idAgent')), 'queueId' : Number(e.dataTransfer.getData('idQueue'))});
                      return false;
                  },
                  false
              );
            }
        };
  }).directive('draggableGroup', function() {
      return {
          replace:true,
          link : function(scope, element) {
              var el = element[0];
              el.draggable = true;
              el.addEventListener(
                  'dragstart',
                  function(e) {
                      e.dataTransfer.effectAllowed = 'move';
                      e.dataTransfer.setData('groupId', scope.group.id);
                      e.dataTransfer.setData('queueId', scope.queue.id);
                      e.dataTransfer.setData('penalty', scope.groupOfGroups.penalty);
                      e.dataTransfer.setData('clone', e.ctrlKey);
                      this.style.opacity = '0.6';
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'dragend',
                  function(e) {
                      this.style.opacity = '1';
                      return false;
                  },
                  false
              );
          }
      };
  }).directive('droppableGroup', function($rootScope) {
      return {
          replace:true,
          link: function(scope, element) {
              var el = element[0];
              el.addEventListener(
                  'dragover',
                  function(e) {
                      e.dataTransfer.dropEffect = 'move';
                      if (e.preventDefault) e.preventDefault();
                      this.style.backgroundColor = "Yellow";
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'dragleave',
                  function(e) {
                      e.dataTransfer.dropEffect = 'move';
                      if (e.preventDefault) e.preventDefault();
                      this.style.backgroundColor = "";
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'drop',
                  function(e) {
                      if (e.stopPropagation) e.stopPropagation();
                      this.style.backgroundColor = "";
                      scope.agentInGroup(Number(e.dataTransfer.getData('groupId')),
                        Number(e.dataTransfer.getData('queueId')),
                        Number(e.dataTransfer.getData('penalty')),
                        scope.queue.id,
                        scope.groupOfGroups.penalty,
                        (e.dataTransfer.getData('clone') === 'true')
                      );
                      return false;
                  },
                  false
              );
            }
        };
  }).directive('droppableGroupTrash', function($rootScope) {
      return {
          replace:true,
          link: function(scope, element) {
              var el = element[0];
              el.addEventListener(
                  'dragover',
                  function(e) {
                      e.dataTransfer.dropEffect = 'move';
                      if (e.preventDefault) e.preventDefault();
                      this.style.backgroundColor = "Red";
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'dragleave',
                  function(e) {
                      e.dataTransfer.dropEffect = 'move';
                      // allows us to drop
                      if (e.preventDefault) e.preventDefault();
                      this.style.backgroundColor = "";
                      return false;
                  },
                  false
              );
              el.addEventListener(
                  'drop',
                  function(e) {
                      if (e.stopPropagation) e.stopPropagation();
                      this.style.backgroundColor = "";
                      scope.removeAgentGroupFromQueueGroup(Number(e.dataTransfer.getData('groupId')),
                        Number(e.dataTransfer.getData('queueId')),
                        Number(e.dataTransfer.getData('penalty'))
                      );
                      return false;
                  },
                  false
              );
            }
        };
  }).directive('d3Queue', ['$filter',function($filter) {
    return {
        restrict : 'E' ,
        scope: {
            data: '=',
            ratio: '='
        },
        link : function(scope, element) {
            var heightSvg = 165;
            var widthSvg = 165;
            var heightZoom = heightSvg*scope.ratio;
            var widthZoom = widthSvg*scope.ratio;
            var svg = d3.select(element[0]).append("svg").attr("height", heightSvg).attr("width", widthSvg);
            var styleRectangleBackground = "fill:#"+Math.floor(Math.random()*16777215).toString(16)+";opacity:0.8;";


            scope.render = function(data, widthZoom, heightZoom) {
                function Indicator(svgContainer, size, counter, label, fontSize, formatter) {
                    var position = {};
                    var className = 'queueIndicator';
                    var textStyle = "fill:white;text-anchor:middle; font-size:"+fontSize+"px;";
                    var roundCorner = 10;
                    var drawRect = function() {
                        svgContainer.append("rect").
                        attr("x", position.x).attr("y", position.y).
                        attr("width", size.w).attr("height", size.h).
                        attr("class",className).
                        attr("rx",roundCorner).attr("ry",roundCorner);
                    };
                    var drawCounter = function() {
                        var texteTpsMax = svgContainer.append("text").attr("style", textStyle);
                        texteTpsMax.append("tspan").attr("x", position.x + size.w/2).attr("y", position.y + size.h/2  -(fontSize/2)).text(label);
                        texteTpsMax.append("tspan").attr("x", position.x + size.w/2).attr("y", position.y + size.h/2  + fontSize).text(formatter(counter));
                    };
                    return {
                        drawOn : function(pos) {
                            position = pos;
                            drawRect();
                            drawCounter();
                        }
                    };
                }
                d3.select(element[0]).select("g").remove();
                var svgContainer = svg.append("g");
                var marge = 5;
                var padding = 10;
                var indicatorSize = {w : (widthZoom-marge-(2*padding))/2, h : (heightZoom-marge-(2*padding))/2};
                var width = (widthZoom-marge-(2*padding))/2;
                var height = (heightZoom-marge-(2*padding))/2;
                var fontSize = 15*scope.ratio;
                function Pos() {
                    var Itop = padding, Ileft= padding, Iright = width+marge+padding, Ibottom = height+marge+padding;
                    return {
                        topLeft : {x:Ileft, y:Itop},
                        topRight : {x:Iright, y:Itop},
                        bottomLeft : {x:Ileft,y:Ibottom},
                        bottomRight : {x:Iright, y:Ibottom}
                    };
                }
                var pos = Pos();
                var drawBackground = function(style) {
                    svgContainer.append("rect")
                        .attr("x", 2.5*padding)
                        .attr("y", 2.5*padding)
                        .attr("width", widthZoom-(5*padding))
                        .attr("height", heightZoom-(5*padding))
                        .attr("style", styleRectangleBackground)
                        .attr("transform", "rotate(45,"+(width+(marge/2)+padding)+","+(height+(marge/2)+padding)+")");
                };
                var percentFormatter = function(counter) {
                    if (counter) {
                        return $filter('number')(counter,1) + " %";
                    }
                    else {
                        return "- %";
                    }
                };
                var noFormat = function(counter){return (counter || "0");};

                drawBackground(svgContainer,styleRectangleBackground);

                Indicator(svgContainer,indicatorSize,data.PercentageAnsweredBefore15,"R 15s",fontSize,percentFormatter).drawOn(pos.topLeft);
                Indicator(svgContainer,indicatorSize,data.AvailableAgents,"Dispo",fontSize,noFormat).drawOn(pos.topRight);
                Indicator(svgContainer,indicatorSize,data.WaitingCalls,"Att",fontSize,noFormat).drawOn(pos.bottomRight);
                Indicator(svgContainer,indicatorSize,data.PercentageAbandonnedAfter15,"A 15s",fontSize,percentFormatter).drawOn(pos.bottomLeft);
            };
            scope.$watch('data', function(){
                scope.render(scope.data, widthZoom, heightZoom);
            }, true);
            scope.$watch('ratio', function(){
                heightZoom = heightSvg*scope.ratio;
                widthZoom = widthSvg*scope.ratio;
                d3.select(element[0]).select("svg").remove();
                svg = d3.select(element[0]).append("svg").attr("height", heightZoom).attr("width", widthZoom);
                scope.render(scope.data, widthZoom, heightZoom);
            }, true);
        }
    };
}]).directive('d3Agent', function() {
    return {
        restrict : 'E' ,
        scope: {
            data: '=',
            ratio: '='
        },
        link : function(scope, element) {
            var heightSvg = 50;
            var widthSvg = 65;
            var widthZoom = widthSvg * scope.ratio;
            var heightZoom = heightSvg * scope.ratio;
            var widthZoomOrigin = widthZoom;
            var roundCorner = 5;
            var nameMaxLen = 7;
            var svg = d3.select(element[0]).append("svg").attr("height", heightSvg).attr("width", widthSvg);

            scope.render = function(data, widthZoom, heightZoom) {
                var fontSize = 12 * scope.ratio;
                var formatText = function (str, maxLen) {
                    if (str.length > maxLen) {
                        return str.substring(0, maxLen-1) + '.';
                    }
                    else return str;
                };
                d3.select(element[0]).select("g").remove();
                var svgContainer = svg.append("g");
                var padding = 1;
                var agentStyle = "font-size:"+fontSize+"px;";

                svgContainer.append("rect").attr("x",padding).attr("y",3*padding).attr("rx",roundCorner).attr("ry",roundCorner)
                                           .attr("width", widthZoom-(2*padding)).attr("height", heightZoom-(6*padding))
                                           .attr("class", data.state + " AgentBox");
                var labelPos = {};
                    labelPos.top = (2*padding) + fontSize*1.1;
                    labelPos.middle = (2*padding) + fontSize*2.5;
                    labelPos.bottom = (2*padding) + fontSize*3.5;
                var drawAgentLabel = function(text, ypos) {
                    svgContainer.append("text").attr("style", agentStyle)
                                  .append("tspan").attr("x", widthZoom/2).attr("y",ypos)
                                  .attr("class","AgentText").text(text);
                };
                drawAgentLabel(data.phoneNb,labelPos.top);
                var toDisplay = data.lastName ? formatText(data.lastName.toUpperCase(),nameMaxLen) : "";
                drawAgentLabel(toDisplay,labelPos.middle);
                toDisplay = data.lastName ? formatText(data.firstName,nameMaxLen) : "";
                drawAgentLabel(toDisplay,labelPos.bottom);
            };

            scope.$watch('data.state', function(){
                scope.render(scope.data, widthZoom, heightZoom);
            }, true);

            scope.$watch('ratio', function(){
                widthZoom = widthSvg * scope.ratio;
                heightZoom = heightSvg * scope.ratio;
                widthZoomOrigin = widthZoom;
                d3.select(element[0]).select("svg").remove();
                svg = d3.select(element[0]).append("svg").attr("height", heightZoom).attr("width", widthZoom);
                scope.render(scope.data, widthZoom, heightZoom);
            }, true);
        }
    };
 }).directive('d3EmptyGroup', function() {
    return {
        restrict : 'E',
        scope : {
            ratio: '='
        },
        link : function(scope, element) {
            var normHeight = 20;
            var heightSvg = normHeight * scope.ratio;
            var widthSvg = 3 * scope.ratio;
            var svgContainer = d3.select(element[0]).append("svg").attr("height", heightSvg).attr("width", widthSvg);
            svgContainer.append("rect").attr("x",0).attr("y",0).attr("width",widthSvg ).attr("height", heightSvg);

            scope.$watch('ratio', function(){
                heightSvg = normHeight * scope.ratio;
                widthSvg = 5 * scope.ratio;
                d3.select(element[0]).select("svg").remove();
                svgContainer = d3.select(element[0]).append("svg").attr("height", heightSvg).attr("width", widthSvg);
                svgContainer.append("rect").attr("x",5).attr("y",5).attr("width",widthSvg -5).attr("height", heightSvg-5).attr("style", "fill : transparent");
            }, true);
        }
     };
}).directive('d3Group', function() {
      return {
          restrict : 'E',
          scope : {
            data: '=',
            ratio: '='
          },
          link : function(scope, element) {
              var normHeight = 50;
              var heightSvg = normHeight;
              var widthSvg = 180;
              var padding = 1;
              var roundCorner = 5;
              var fontSize = 15;
              var svgContainer = d3.select(element[0]).append("svg").attr("height", heightSvg+5).attr("width", widthSvg+5);

              svgContainer.append("rect").attr("x",padding+3).attr("y",3*padding+3).attr("rx",roundCorner).attr("ry",roundCorner)
                                           .attr("width", widthSvg-(2*padding)).attr("height", heightSvg-(6*padding))
                                           .attr("class", "AgentGroupBoxShadow");
              svgContainer.append("rect").attr("x",padding).attr("y",3*padding).attr("rx",roundCorner).attr("ry",roundCorner)
                                           .attr("width", widthSvg-(2*padding)).attr("height", heightSvg-(6*padding))
                                           .attr("class", "AgentGroupBox");

                var labelPos = {};
                    labelPos.top = (2*padding) + fontSize*1.1;
                    labelPos.middle = (2*padding) + fontSize*2.5;
                    labelPos.bottom = (2*padding) + fontSize*3.5;
                var groupStyle = "font-size:"+fontSize+"px;";

                var drawLabel = function(text, ypos) {
                    svgContainer.append("text").attr("style", groupStyle)
                                  .append("tspan").attr("x", widthSvg/2).attr("y",ypos)
                                  .attr("class","AgentGroupText").text(text);
                };
                drawLabel(scope.data.nbOfAgents,labelPos.top);
                drawLabel(scope.data.name,labelPos.middle);

          }
       };
  });
