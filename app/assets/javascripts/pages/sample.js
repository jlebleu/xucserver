$(function() {
    console.log("sample handlers");
    var agentStateEventHandler = function(agentState) {
        $('#agent_events').prepend(
                '<li class="list-group-item">' + new Date().toLocaleString() + ": " + JSON.stringify(agentState) + '</li>');
        $('#xuc_agentData').val("cause :  " + agentState.cause);
        if (agentState.name !== "AgentLoggedOut") {
            loginHandler(agentState);
        }
        switch (agentState.name) {
        case "AgentReady":
            readyHandler();
            break;
        case "AgentOnPause":
            onPauseHandler();
            break;
        case "AgentOnCall":
            onCallHandler(agentState);
            break;
        case "AgentOnWrapup":
            onWrapupHandler();
            break;
        case "AgentLoggedOut":
            logoutHandler();
            break;
        }
    };
    var loginHandler = function(event) {
        $('#xuc_agentStatus').val("Logged in");
        $('#xuc_agentPhoneNumber').val(event.phoneNb);
    };

    var logoutHandler = function() {
        $('#xuc_agentStatus').val("Logged out");
    };

    var readyHandler = function() {
        $('#xuc_agentStatus').val("Ready");
    };
    var onPauseHandler = function() {
        $('#xuc_agentStatus').val("Paused");
    };
    var onCallHandler = function(state) {
        $('#xuc_agentStatus').val("On Call");
        $('#xuc_agentData').val("Acd : " + state.acd + " dir : " + state.direction + " Type : " + state.callType);
    };
    var onWrapupHandler = function() {
        $('#xuc_agentStatus').val("Wrapup");
    };
    var usersStatusesHandler = function(statuses) {
        $.each(statuses, function(key, item) {
            $("#userPresence")
                    .append("<li><a href=\"#\" id=\"" + item.name + "\">" + item.longName + "</a></li>");
        });
        $('.dropdown-menu li a').click(function(event) {
            Cti.changeUserStatus($(this).attr('id'));
        });
    };

    var userStatusHandler = function(event) {
        console.log("user Status Handler " + JSON.stringify(event));
        $('#xuc_userStatus').val(event.status);
    };

    var phoneStatusHandler = function(event) {
        console.log("phone Status Handler " + JSON.stringify(event));
        $('#xuc_phoneStatus').val(event.status);
    };

    var linkStatusHandler = function(event) {
        console.log("link Status Handler " + JSON.stringify(event));
        if (event.status === "closed") {
            $('#xuc_logon_panel').show();
        }
    };

    var loggedOnHandler = function() {
        console.log("Logged On Handler ");
        $('#xuc_logon_panel').hide();
    };

    var queueStatisticsHandler = function(event) {
        console.log("queue statistics Handler " + JSON.stringify(event));
        $('#queue_stats').prepend(
                '<li class="list-group-item">' + new Date().toLocaleString() + ": " + JSON.stringify(event) + '</li>');
    };
    var queueConfigHandler = function(queueConfig) {
        $('#queue_config').prepend('<li class="list-group-item">' + JSON.stringify(queueConfig) + '</li>');
    };
    var queueMemberHandler = function(queueMember) {
        $('#queue_member').prepend('<li class="list-group-item">' + JSON.stringify(queueMember) + '</li>');
    };
    var directoryResultHandler = function(directoryResult) {
        console.log("Directory result" + JSON.stringify(directoryResult));
        DirectoryDisplay.prepareTable();
        DirectoryDisplay.displayHeaders(directoryResult.headers);
        DirectoryDisplay.displayEntries(directoryResult.entries);
    };

    var agentConfigHandler = function(agentConfig) {
        console.log("agent config Handler " + JSON.stringify(agentConfig));
        $('#agent_config').prepend('<li class="list-group-item">' + JSON.stringify(agentConfig) + '</li>');
    };
    var agentGroupConfigHandler = function(agentGroups) {
        console.log("agent group config Handler " + JSON.stringify(agentGroups));
        $('#agentgroup_config').prepend('<li class="list-group-item">' + JSON.stringify(agentGroups) + '</li>');
    };

    var agentErrorHandler = function(error) {
        console.log("agent error Handler " + JSON.stringify(error));
        $('#agent_error').html(
                '<p class="text-danger">' + new Date().toLocaleString() + ": " + JSON.stringify(error) + '</p>');
    };
    var errorHandler = function(error) {
        console.log("error Handler " + JSON.stringify(error));
        $('#agent_error').html(
                '<p class="text-danger">' + new Date().toLocaleString() + ": " + JSON.stringify(error) + '</p>');
    };
    var agentDirectoryHandler = function(agentDirectory) {
        console.log("agent directory Handler " + JSON.stringify(agentDirectory));
        $('#agent_directory').prepend('<li class="list-group-item">' + JSON.stringify(agentDirectory) + '</li>');
    };

    var xucserver = window.location.hostname+":"+window.location.port;

    $('#xuc_server').val(xucserver);

    $('#xuc_sign_in').click(
            function(event) {
                var server = $('#xuc_server').val();
                var username = $('#xuc_username').val();
                var password = $('#xuc_password').val();
                var phoneNumber = $('#xuc_phoneNumber').val();
                console.log("sign in " + server + " : " + username + " " + password + " " + phoneNumber);
                var wsurl = "ws://" + server + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                        phoneNumber + "&amp;password=" + password;
                Cti.debugMsg = true;
                Cti.debugHandler = false;
                Cti.setHandler(Cti.MessageType.USERSTATUSES, usersStatusesHandler);
                Cti.setHandler(Cti.MessageType.USERSTATUSUPDATE, userStatusHandler);
                Cti.setHandler(Cti.MessageType.LOGGEDON, loggedOnHandler);
                Cti.setHandler(Cti.MessageType.PHONESTATUSUPDATE, phoneStatusHandler);
                Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, linkStatusHandler);
                Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
                Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
                Cti.setHandler(Cti.MessageType.QUEUELIST, queueConfigHandler);
                Cti.setHandler(Cti.MessageType.QUEUEMEMBER, queueMemberHandler);
                Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, queueMemberHandler);
                Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, directoryResultHandler);

                Cti.setHandler(Cti.MessageType.AGENTCONFIG, agentConfigHandler);
                Cti.setHandler(Cti.MessageType.AGENTLIST, agentConfigHandler);
                Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, agentGroupConfigHandler);
                Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, agentStateEventHandler);
                Cti.setHandler(Cti.MessageType.AGENTERROR, agentErrorHandler);
                Cti.setHandler(Cti.MessageType.ERROR, errorHandler);
                Cti.setHandler(Cti.MessageType.AGENTDIRECTORY, agentDirectoryHandler);

                DirectoryDisplay.init("#directoryresult");
                Cti.WebSocket.init(wsurl, username, phoneNumber);
            });
    $('#xuc_restart').click( function(event) {
        Cti.close();
        var server = $('#xuc_server').val();
        var username = $('#xuc_username').val();
        var password = $('#xuc_password').val();
        var phoneNumber = $('#xuc_phoneNumber').val();
        var wsurl = "ws://" + server + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
                        phoneNumber + "&amp;password=" + password;
        Cti.WebSocket.init(wsurl, username, phoneNumber);

    });
    $('#xuc_login_btn').click(function(event) {
        Cti.loginAgent($('#xuc_agentPhoneNumber').val());
    });
    $('#xuc_logout_btn').click(function(event) {
        Cti.logoutAgent();
    });
    $('#xuc_pause_btn').click(function(event) {
        Cti.pauseAgent();
    });
    $('#xuc_unpause_btn').click(function(event) {
        Cti.unpauseAgent();
    });
    $('#xuc_subscribe_to_queue_stats_btn').click(function(event) {
        Cti.subscribeToQueueStats();
    });
    $('#xuc_answer_btn').click(function(event) {
        Cti.answer();
    });
    $('#xuc_hangup_btn').click(function(event) {
        Cti.hangup();
    });
    $('#xuc_search_btn').click(function(event) {
        Cti.searchDirectory($("#xuc_destination").val());
    });
    $('#xuc_enable_dnd_btn').click(function(event) {
        Cti.dnd(true);
    });
    $('#xuc_disable_dnd_btn').click(function(event) {
        Cti.dnd(false);
    });
    $('#xuc_dial_btn').click(function(event) {
        Cti.dial($("#xuc_destination").val());
    });
    $('#xuc_direct_transfer_btn').click(function(event) {
        Cti.directTransfer($("#xuc_destination").val());
    });
    $('#xuc_attended_transfer_btn').click(function(event) {
        Cti.attendedTransfer($("#xuc_destination").val());
    });
    $('#xuc_complete_transfer_btn').click(function(event) {
        Cti.completeTransfer();
    });
    $('#xuc_cancel_transfer_btn').click(function(event) {
        Cti.cancelTransfer();
    });
    $('#xuc_get_config_queues').click(function(event) {
        $('#queue_config').html("");
        Cti.getList("queue");
    });
    $('#xuc_get_config_queuemembers').click(function(event) {
        Cti.getList("queuemember");
    });
    $('#xuc_get_config_agents').click(function(event) {
        Cti.getList("agent");
    });
    $('#xuc_get_config_agentgroups').click(function(event) {
        Cti.getList("agentgroup");
    });
    $('#xuc_get_agentstates').click(function(event) {
        Cti.getAgentStates();
    });
    $('#xuc_subscribe_to_agent_event_btn').click(function(event) {
        Cti.subscribeToAgentEvents();
    });
    $('#xuc_get_agent_directory').click(function(event) {
        Cti.getAgentDirectory();
    });
    $('#xuc_set_agent_queue_btn').click(function(event) {
        Cti.setAgentQueue($("#xuc_agentId").val(), $("#xuc_queueId").val(), $("#xuc_penalty").val());
    });
    $('#xuc_remove_agent_from_queue_btn').click(function(event) {
        Cti.removeAgentFromQueue($("#xuc_agentId").val(),$("#xuc_queueId").val());
    });
});
