$(function() {
    console.log("sample handlers");

    var loginHandler = function(event) {
        $('#xuc_agentStatus').val("Logged in");
        $('#xuc_agentPhoneNumber').html('<p class="glyphicon glyphicon-earphone"></p>&nbsp;'+event.phoneNb);
        $('#xuc_logon_panel').hide("slow");
         $("#xuc_table_queues").html("");
        Cti.subscribeToAgentEvents();
        Cti.subscribeToQueueStats();
        Cti.getAgentStates();
    };

    var logoutHandler = function() {
        console.log("logout Handler ");
        $('#xuc_agentStatus').html('<p class="glyphicon glyphicon-briefcase"></p>&nbsp;Logged out');
        $('#xuc_togglelogin_btn').children().html("Login");
        $('#xuc_logon_panel').show("slow");
    };

    var readyHandler = function() {
        console.log("ready Handler ");
        $('#xuc_agentStatus').html('<p class="glyphicon glyphicon-briefcase"></p>&nbsp;Ready');
        $('#xuc_togglelogin_btn').children().html("Logout");
    };

    var pausedHandler = function() {
        console.log("paused Handler ");
        $('#xuc_agentStatus').html('<p class="glyphicon glyphicon-briefcase"></p>&nbsp;Paused');
        $('#xuc_togglelogin_btn').children().html("Logout");
    };

    var queueConfigHandler = function(event) {

        if(queuesFilter.length > 0) {
            if(queuesFilter.indexOf(event.id) >=0)

              if($("#xuc_queue_"+event.id).length === 0) {
                $("#xuc_table_queues").append("<tr id='xuc_queue_"+event.id+"'></tr>");
                $("#xuc_queue_"+event.id).append("<td id='xuc_queue_"+event.id+"_name'>" + event.displayName+"</td>" +
                    "<td id='xuc_queue_"+event.id+"_EWT'></td>" +
                    "<td id='xuc_queue_"+event.id+"_AvailableAgents'></td>" +
                    "<td id='xuc_queue_"+event.id+"_TalkingAgents'></td>");
              } else {
                 $("#xuc_queue_"+event.id+"_name").html(event.displayName);
              }

              Cti.getQueueStatistics(event.id,0,0);

            }
        };

    var queueStatisticsHandler = function(event) {
        console.log("QUEUE STAT" + JSON.stringify(event));
        if(queuesFilter.length > 0) {
            if(queuesFilter.indexOf(event.queueId) >=0) {
                $.each(event.counters, function(index, counter) {
                    $("#xuc_queue_"+event.queueId+"_"+counter.statName).html(counter.value);
                });
            }
        }
    };


    var agentStateEventHandler = function(agentState) {
        if(agentState.phoneNb == phoneNumber) {
           queuesFilter = agentState.queues;
           Cti.getConfig("queue");
        }
    };

    var agentErrorHandler = function(error) {
        console.log("agent error Handler " + JSON.stringify(error));
        $('#agent_error').html(
                '<p class="text-danger">' + new Date().toLocaleString() + ": " + JSON.stringify(error) + '</p>');
    };

     var phoneNumber = 0;
     var queuesFilter = [];

    $('#xuc_sign_in').click(
            function(event) {
                var server = $('#xuc_server').val();
                var username = $('#xuc_username').val();
                var password = $('#xuc_password').val();
                phoneNumber = $('#xuc_phoneNumber').val();
                console.log("sign in " + server + " : " + username + " " + password + " " + phoneNumber);
                var wsurl = "ws://" + server + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" + phoneNumber + "&amp;password=" + password;
                Cti.debugMsg = true;
                Cti.debugHandler = false;
                Cti.setHandler(Cti.MessageType.AGENTLOGIN, loginHandler);
                Cti.setHandler(Cti.MessageType.AGENTLOGOUT, logoutHandler);
                Cti.setHandler(Cti.MessageType.AGENTPAUSED, pausedHandler);
                Cti.setHandler(Cti.MessageType.AGENTREADY, readyHandler);
                Cti.setHandler(Cti.MessageType.QUEUECONFIG, queueConfigHandler);
                Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, queueStatisticsHandler);
                Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, agentStateEventHandler);
                Cti.setHandler(Cti.MessageType.AGENTERROR, agentErrorHandler);

                DirectoryDisplay.init("#directoryresult");
                Cti.WebSocket.init(wsurl, username, phoneNumber);

                if($('#xuc_agentLogin').contents().length < 2) $('#xuc_agentLogin').append(" " + username);
                else $('#xuc_agentLogin').contents()[1].textContent = " " + username;

                $('#xuc_togglelogin_btn').children().html("Login");
                $('#xuc_togglepause_btn').children().html("Pause");

            });

    $('#xuc_togglelogin_btn').click(function(event) {
         Cti.toggleAgentLogin();
    });

    $('#xuc_togglepause_btn').click(function(event) {
            if($('#xuc_agentStatus').html().indexOf("Ready") >=0) {
                $('#xuc_togglepause_btn').children().html("Unpause");
                Cti.pauseAgent();
            } else {
                $('#xuc_togglepause_btn').children().html("Pause");
                Cti.unpauseAgent();
            }
    });
 });
