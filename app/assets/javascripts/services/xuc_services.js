
var xucServices = angular.module('xuc.services', []);

 xucServices.filter('queueTime', function() {
    return function(seconds) {
           if (typeof seconds == "undefined" || seconds == "-")
               return "-";
           else if (seconds < 60)
               return seconds;
           if (seconds < 3600)
               return moment().hour(0).minute(0).seconds(0).add('seconds', seconds).format("m:ss");
           return moment().hour(0).minute(0).seconds(0).add('seconds', seconds).format("H:mm:ss");
        };
  });

xucServices.factory('XucQueue',['$timeout','$rootScope',function($timeout, $rootScope){
    return Xuc.Queue($timeout, $rootScope);
}]);

var Xuc = {};

Xuc.Queue = function($timeout, $rootScope) {
    'use strict';
    var NoLongestWaitTime = "-";

    var queues = [];
    var loadQueues = function() {
        Cti.getList("queue");
        subscribeToQueueStats();
    };
    var subscribeToQueueStats = function() {
        Cti.subscribeToQueueStats();
    };

    var _queueReplace = function(queueConfig) {
        var replaced = false;
        angular.forEach(queues, function(queue, key) {
            if (queue.id === queueConfig.id) {
                angular.forEach(queueConfig, function(value, property) {
                    queue[property] = value;
                });
                replaced = true;
            }
        });
        return replaced;
    };

    this.onQueueConfig = function(queueConfig) {
        queueConfig.LongestWaitTime = NoLongestWaitTime;
        queueConfig.WaitingCalls = 0;
        var replaced = _queueReplace(queueConfig);
        if (_queueReplace(queueConfig) === false) {
            queues.splice(1, 0, queueConfig);
        }
    };

    this.onQueueList = function(queueList) {
        for (var i = 0; i < queueList.length; i++) {
            this.onQueueConfig(queueList[i]);
        }
        $rootScope.$broadcast('QueuesLoaded');
    };
    this.onQueueStatistics = function(queueStatistics) {
        var updateCounters = function(counters, queue) {
            angular.forEach(counters, function(counter, key) {
                queue[counter.statName] = counter.value;
            });
            if (queue.WaitingCalls === 0) {
                queue.LongestWaitTime = NoLongestWaitTime;
            }
        };
        var queueSelector = function(i) {return queues[i].id === queueStatistics.queueId;};
        if (typeof queueStatistics.queueId === 'undefined') {
            queueSelector = function(i) {return queues[i].name === queueStatistics.queueRef;};
        }
        for (var j = 0; j < queues.length; j++) {
            if (queueSelector(j)) {
                updateCounters(queueStatistics.counters,queues[j]);
            }
        }
        $rootScope.$broadcast('QueueStatsUpdated');
    };

    this.start = function() {
        loadQueues();
    };

    this.updateQueues = function(updateQueueFrequency) {
        for (var i = 0; i < queues.length; i++) {
            if (queues[i].LongestWaitTime >= 0 && queues[i].WaitingCalls !== 0) {
                queues[i].LongestWaitTime += updateQueueFrequency;
            } else {
                queues[i].LongestWaitTime = "-";
            }
        }
    };

    this.getQueue = function(queueId) {
        for (var j = 0; j < queues.length; j++) {
            if (queues[j].id === queueId) {
                return queues[j];
            }
        }
    };

    this.getQueues = function() {
        return queues;
    };

    this.getQueuesExcept = function(excludeQueues) {
        var acceptQueue = function(queue) {
            for (var j = 0; j < excludeQueues.length; j++) {
                if (excludeQueues[j].id === queue.id) {
                    return false;
                }
            }
            return true;
        };
        return queues.filter(acceptQueue);
    };

    Cti.setHandler(Cti.MessageType.QUEUECONFIG, this.onQueueConfig.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUELIST, this.onQueueList.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUESTATISTICS, this.onQueueStatistics.bind(this));
    return this;
};
xucServices.factory('XucAgent',['$rootScope','XucQueue','$filter',function($rootScope, xucQueue, $filter){
    return Xuc_Agent($rootScope, xucQueue, $filter);
}]);

var Xuc_Agent = function($rootScope, xucQueue, $filter) {
    var agents = [];
    var agentStatesWatingForConfig = [];
    var userStatuses = [];

    var newAgent = function(id) {
        var agent =  {
            'id' : id,
            'lastName' : '',
            'firstName' : '',
            'queueMembers' : [],
            'state' : 'AgentLoggedOut',
            'status' : '-'
        };
        agents.push(agent);
        return(agent);
    };
    var newAgentStatesWatingForConfig = function(id) {
        var agent =  {
            'id' : id,
            'lastName' : '',
            'firstName' : '',
            'queueMembers' : [],
            'state' : 'AgentLoggedOut',
            'status' : '-'
        };
        agentStatesWatingForConfig.push(agent);
        return(agent);
    };

    var updateAgent = function(agentUpdate,agent) {
        angular.forEach(agentUpdate, function(value, property) {
            agent[property] = value;
        });
    };

    var updateQueueMember = function(queueMember) {
        var agent = this.getAgent(queueMember.agentId) || newAgentStatesWatingForConfig(queueMember.agentId);
        if (queueMember.penalty >= 0) {
            agent.queueMembers[queueMember.queueId] = {};
            agent.queueMembers[queueMember.queueId] = queueMember.penalty;
        }
        else {
            delete agent.queueMembers[queueMember.queueId];
        }
    };

    this.buildMoment = function(since) {
        var start = moment().add('seconds', -(since));
        return {
            'momentStart' : start,
            'timeInState' : moment().countdown(start)
        };
    };

    this.onUserStatuses = function(usrtatuses) {
        angular.forEach(usrtatuses, function (userStatus, keyUserStatus) {
            userStatuses[userStatus.name] = userStatus.longName;
        });
    };
    this.getAgents = function() {
        return agents;
    };


    this.getAgent = function(agentId) {
        for (var j = 0; j < agents.length; j++) {
            if (agents[j].id === agentId) {
                return agents[j];
            }
        }
        return null;
    };
    this.getAndCopyAgentStatesWaitingForConfig = function(agentId) {
        for (var j = 0; j < agentStatesWatingForConfig.length; j++) {
            if (agentStatesWatingForConfig[j].id === agentId) {
                var ag = agentStatesWatingForConfig[j];
                agents.push(ag);
                agentStatesWatingForConfig.splice(j,1);
                return ag;
            }
        }
        return null;
    };
    this.getAgentsNotInQueue = function(queueId) {
        var ags = [];
        angular.forEach(agents, function(agent, index) {
            if(typeof agent.queueMembers[queueId] === 'undefined') {
                ags.push(agent);
            }
        });
        return ags;
    };
    this.getAgentsInQueue = function(queueId) {
        var ags = [];
        angular.forEach(agents, function(agent, index) {
            if(typeof agent.queueMembers[queueId] !== 'undefined') {
                ags.push(agent);
            }
        });
        return ags;
    };
    this.getAgentsInQueues = function(queueName) {
        var agentsInQueues = [];

        var queues = xucQueue.getQueues();

        queues = $filter('filter')(queues, { name :queueName});


        angular.forEach(queues, function(queue, index) {
            angular.forEach(this.getAgentsInQueue(queue.id), function(agent,index){
                if (agentsInQueues.indexOf(agent) < 0) agentsInQueues.push(agent);
            });
        });
        return agentsInQueues;
    };
    this.onAgentList  = function(agentList) {
        for (var i = 0; i < agentList.length; i++) {
            this.onAgentConfig(agentList[i]);
        }
        angular.forEach(agents, function(agent, index) {
            if(agent.firstName === '') {
                agents.splice(index,1);
            }
        });
        $rootScope.$broadcast('AgentsLoaded');
    };
    this.onAgentConfig = function(agentConfig) {
        var agent = this.getAgent(agentConfig.id) || (getAndCopyAgentStatesWaitingForConfig(agentConfig.id) || newAgent(agentConfig.id));
        updateAgent(agentConfig,agent);
    };

    this.onAgentState = function(agentState) {
        var agent = this.getAgent(agentState.agentId) || newAgentStatesWatingForConfig(agentState.agentId);
        agent.state = agentState.acd ? 'AgentOnAcdCall': agentState.name;
        agent.phoneNb =  agentState.phoneNb || '';
        agent.phoneNb = (agent.phoneNb === 'null' ? '' : agent.phoneNb);
        agent.queues = agentState.queues;
        agent.status = userStatuses[agentState.cause] || '-';
        var mt = this.buildMoment(agentState.since);
        agent.momentStart = mt.momentStart;
        agent.timeInState = mt.timeInState;
        $rootScope.$broadcast('AgentStateUpdated',agent.id);
    };

    this.onQueueMember = function(queueMember) {
        updateQueueMember(queueMember);
        $rootScope.$broadcast('QueueMemberUpdated', queueMember.queueId);
    };

    this.onQueueMemberList = function(queueMemberList) {
        var queueIds = [];
        for (var i = 0; i < queueMemberList.length; i++) {
            updateQueueMember(queueMemberList[i]);
            queueIds[queueMemberList[i].queueId] = true;
        }
        angular.forEach(queueIds, function(done, queueId) {
            $rootScope.$broadcast('QueueMemberUpdated', queueId);
        });
    };

    this.start = function() {
        console.log('********************** agent service starting *********************');
        Cti.getList("agent");
        Cti.getList("queuemember");
        Cti.getAgentStates();
        Cti.subscribeToAgentEvents();
    };

    this.canLogIn = function(agentId){
        return (this.getAgent(agentId).state === 'AgentLoggedOut');
    };

    this.login = function(agentId) {
        Cti.loginAgent(this.getAgent(agentId).phoneNb,agentId);
    };

    this.canLogOut = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentReady');
    };
    this.logout = function(agentId) {
        Cti.logoutAgent(agentId);
    };
    this.canPause = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentReady');
    };
    this.pause = function(agentId) {
        Cti.pauseAgent(agentId);
    };
    this.canUnPause = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentOnPause');
    };
    this.unpause = function(agentId) {
        Cti.unpauseAgent(agentId);
    };
    this.canListen = function(agentId) {
        return (this.getAgent(agentId).state === 'AgentOnAcdCall');
    };
    this.listen = function(agentId) {
        Cti.listenAgent(agentId);
    };
    this.updateQueues = function(agentId, updatedQueues) {
        var agent = this.getAgent(agentId);
        var updateQueue = function(queueToUpdate) {
            if(queueToUpdate.penalty !== agent.queueMembers[queueToUpdate.id])
                Cti.setAgentQueue(agentId,queueToUpdate.id,queueToUpdate.penalty);
        };
        var updateOrRemoveQueue = function(queueId) {
            for (var j = 0; j < updatedQueues.length; j++) {
                if (updatedQueues[j].id === queueId) {
                  updateQueue(updatedQueues[j]);
                  updatedQueues.splice(j,1);
                  return;
                }
            }
            Cti.removeAgentFromQueue(agentId,queueId);
        };
        var addAgentToQueues = function() {
            for (var j = 0; j < updatedQueues.length; j++) {
                if (typeof agent.queueMembers[updatedQueues[j].id] === 'undefined')
                    Cti.setAgentQueue(agentId,updatedQueues[j].id,updatedQueues[j].penalty);
            }
        };
        angular.forEach(agent.queueMembers, function (penalty, queueId) {
            updateOrRemoveQueue(queueId);
        });
        addAgentToQueues();

    };

    Cti.setHandler(Cti.MessageType.AGENTCONFIG, this.onAgentConfig.bind(this));
    Cti.setHandler(Cti.MessageType.AGENTLIST, this.onAgentList.bind(this));
    Cti.setHandler(Cti.MessageType.USERSTATUSES, this.onUserStatuses.bind(this));
    Cti.setHandler(Cti.MessageType.AGENTSTATEEVENT, this.onAgentState.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUEMEMBER, this.onQueueMember.bind(this));
    Cti.setHandler(Cti.MessageType.QUEUEMEMBERLIST, this.onQueueMemberList.bind(this));

    return this;

};

xucServices.factory('XucGroup',['$rootScope','XucAgent','$filter',function($rootScope, xucAgent, $filter){
    return Xuc_Group($rootScope, xucAgent, $filter);
}]);

var Xuc_Group = function($rootScope, xucAgent, $filter) {
    var MAXPENALTY = 20;

    var groups = [];

    var _onAgentGroupList = function(agentGroups) {
        groups = agentGroups;
    };

    var _getGroups = function() {
        return groups;
    };

    var _getGroup = function(groupId) {
        for (var j = 0; j < groups.length; j++) {
            if (groups[j].id === groupId) {
                return groups[j];
            }
        }
        return null;
    };
    var _getAvailableGroups = function(queueId, penalty) {
        var availableGroups = [];
        var availableAgents = xucAgent.getAgentsNotInQueue(queueId);

        angular.forEach(availableAgents, function(agent, key) {
            var group = _getGroup(agent.groupId);
            if (this.indexOf(group) <0) this.push(_getGroup(agent.groupId));
        }, availableGroups);

        return availableGroups;
    };
    var _getGroupsForAQueue = function(queueId) {
        var queueGroups = [];
        var agents = xucAgent.getAgentsInQueue(queueId);

        var initGroup = function(penalty) {
            if (typeof queueGroups[penalty] === 'undefined') queueGroups[penalty] = {'penalty' : penalty, 'groups' : []};
            if (!queueGroups[penalty-1] && penalty > 0) initGroup(penalty-1);
        };
        var getGroup = function(penalty) {
            if (typeof queueGroups[penalty] === 'undefined') initGroup(penalty);
            return queueGroups[penalty];
        };
        var getVGroup = function(groupOfGroup, groupId) {
            for (var j = 0; j < groupOfGroup.groups.length; j++) {
                if (groupOfGroup.groups[j].id === groupId) {
                    return groupOfGroup.groups[j];
                }
            }
            var group = angular.copy(_getGroup(groupId));
            group.nbOfAgents = 0;
            group.agents = [];
            groupOfGroup.groups.push(group);
            return group;
        };

        var addToGroup = function(groupOfGroup, agent) {
            var group = getVGroup(groupOfGroup, agent.groupId);
            group.nbOfAgents = group.nbOfAgents + 1;
            group.agents.push(agent);
        };

        var buildGroups = function() {
            angular.forEach(agents, function (agent, keyAgent) {
                if (typeof agent.queueMembers[queueId] !== 'undefined' && typeof agent.groupId !== 'undefined') {
                    var groups = getGroup(agent.queueMembers[queueId]);
                    addToGroup(groups,agent);
                    if (agent.queueMembers[queueId]+1 < MAXPENALTY) initGroup(agent.queueMembers[queueId]+1);
                }
            });
        };
        initGroup(0);
        buildGroups();
        return queueGroups;
    };
    var _moveAgentsInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        Cti.moveAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
    };
    var _addAgentsInGroup = function(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty) {
        Cti.addAgentsInGroup(groupId, fromQueueId, fromPenalty, toQueueId, toPenalty);
    };
    var _removeAgentGroupFromQueueGroup = function(groupId, queueId, penalty) {
        Cti.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);
    };
    var _addAgentsNotInQueueFromGroupTo = function(groupId, queueId, penalty) {
        Cti.addAgentsNotInQueueFromGroupTo(groupId, queueId, penalty);
    };
    var _start = function() {
        Cti.getList("agentgroup");
    };

    Cti.setHandler(Cti.MessageType.AGENTGROUPLIST, _onAgentGroupList);

    return {
        getGroups : _getGroups,
        onAgentGroupList : _onAgentGroupList,
        start : _start,
        getGroup : _getGroup,
        getGroupsForAQueue : _getGroupsForAQueue,
        getAvailableGroups : _getAvailableGroups,
        moveAgentsInGroup : _moveAgentsInGroup,
        addAgentsInGroup : _addAgentsInGroup,
        removeAgentGroupFromQueueGroup: _removeAgentGroupFromQueueGroup,
        addAgentsNotInQueueFromGroupTo: _addAgentsNotInQueueFromGroupTo
    };
};
