import com.joescii.SbtJasminePlugin._
import com.typesafe.sbt.SbtSite.site
import com.typesafe.sbt.packager.linux.LinuxPackageMapping
import com.typesafe.sbt.site.SphinxSupport._
import sbt.Keys._
import sbt._
import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._
import com.typesafe.sbt.packager.archetypes.ServerLoader.SystemV


object ApplicationBuild extends Build {

  val appName = "xuc"
  val appVersion = "2.4.10"
  val appOrganisation = "xivo"

  lazy val xucmod = ProjectRef(file("../xucmod"), "xucmod")

  val main = Project(appName, file("."))
    .enablePlugins(play.PlayScala)
    .settings(
      name := appName,
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      resolvers ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      publishArtifact in(Compile, packageDoc) := false,
      publishArtifact in packageDoc := false
    )
    .settings(Jasmine.settings: _*)
    .settings(CustomDebianSettings.settings: _*)
    .settings(CustomDebianSettings.debianMappings: _*)
    .settings(
      testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u", "target/test-reports")
    )
    .settings(
      Site.settings: _*
    )
    .settings(
      DockerSettings.settings: _*
    )
    //.dependsOn(xucmod)
    //.aggregate(xucmod)

  object Site {
    val settings = site.settings ++ site.sphinxSupport() ++ Seq(
      enableOutput in generatePdf in Sphinx := true
    )
  }

  object DockerSettings {
    val settings = Seq(
      maintainer in Docker := "Jean-Yves LEBLEU <jylebleu@avencall.com>",
      dockerBaseImage := "java:openjdk-7u65-jdk",
      dockerExposedPorts in Docker := Seq(9000),
      dockerExposedVolumes in Docker := Seq("/conf"),
      dockerRepository := Some("jlebleu")
    )
  }

  object Jasmine {
    val settings = jasmineSettings ++ Seq(
      appJsDir <+= baseDirectory / "app/assets/javascripts",
      appJsLibDir <+= baseDirectory / "public/javascripts/",
      jasmineTestDir <+= baseDirectory / "test/assets/",
      jasmineConfFile <+= baseDirectory / "test/assets/test.dependencies.js",
      (test in Test) <<= (test in Test) dependsOn (jasmine),
      jasmineEdition := 2
    )
  }

  object CustomDebianSettings {
    val settings = Seq(
      daemonUser in Linux := "xuc",
      serverLoading in Debian := SystemV,
      maintainer in Debian := "Jean-Yves LEBLEU <jylebleu@avencall.com>",
      packageSummary := "Xuc framework",
      packageDescription := """supervision, agent application and samples for Xivo""",
      debianMaintainerScripts  ++= Seq(file("debian/DEBIAN/postrm") -> "postrm",
                                      file("debian/DEBIAN/preinst") -> "preinst",
                                      file("debian/DEBIAN/preinst") -> "postinst",
                                      file("debian/DEBIAN/postinst-useradd") -> "postinst-useradd",
                                      file("debian/DEBIAN/postrm-purge") -> "postrm-purge"
                                      ),
      debianChangelog := Some(file("debian/changelog"))

    )

    val debianMappings = Seq (
      linuxPackageMappings in Debian <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "logs") -> "/var/log/xuc/",(bd / "doc") -> "/usr/share/xuc/")
         withUser "xuc" withGroup "xuc" withPerms "0755")
      },

      linuxPackageMappings in Debian <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "etc/init.d/xuc_init") -> "/etc/init.d/xuc")
         withUser "root" withGroup "root" withPerms "0755")
      },

      linuxPackageMappings in Debian  <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "conf/application.conf") -> "/usr/share/xuc/conf/xuc.conf")
         withUser "xuc" withGroup "xuc" withPerms "0755")
      },

      linuxPackageMappings in Debian  <+= (baseDirectory) map { bd =>
      (packageMapping((bd / "conf/xuc_logger.xml") -> "/usr/share/xuc/conf/xuc_logger.xml")
         withUser "xuc" withGroup "xuc" withPerms "0755")
      },
      linuxPackageMappings in Debian <<= (linuxPackageMappings in Debian) map { mappings =>
        for(LinuxPackageMapping(filesAndNames, meta, zipped) <- mappings) yield {
          val newFilesAndNames = for {
            (file, installPath) <- filesAndNames
            if !installPath.contains("/application.conf")
            if !installPath.contains("/logger.xml")
          } yield file -> installPath
          LinuxPackageMapping(newFilesAndNames, meta, zipped)
        }
      }
    )
  }
}

