import sbt._
import play.Play.autoImport._


object Version {
  val xucmod = "2.4.10"
  val selenium = "2.31.0"
  val htmlcompressor = "1.4"
  val yuicompressor = "2.4.6"
  val closurecompiler = "r1043"
}


object Library {
  val xucmod          =  "xivo"                           %%  "xucmod"            % Version.xucmod
  val selenium        =  "org.seleniumhq.selenium"        %   "selenium-java"     % Version.selenium
  val htmlcompressor  =  "com.googlecode.htmlcompressor"  %   "htmlcompressor"    % Version.htmlcompressor
  val yuicompressor   =  "com.yahoo.platform.yui"         % "yuicompressor"       % Version.yuicompressor
}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.2"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us")
  )

  val runDep = run(
    xucmod,
    htmlcompressor,
    yuicompressor
  )

  val testDep = test(
    selenium
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
